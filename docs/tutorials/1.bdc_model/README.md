﻿# Deploying Behavioural Drift Computation model

In this example, we will simply create and deploy a Behavioural Drift Computation model using GeneSIS.

## Start Behavioural Drift Computation model editor:

First, let's start Behavioural Drift Computation (BDC) model editor by using the following command:

        cd bda-app
        npm start

You should see the following message:

        > bda-app@0.1.0 start D:\Dev\contrats\ENACT\behavioural_drift_analysis\bda-app
        > concurrently "react-scripts start" "nodemon server/index.js"

        [1] [nodemon] 1.19.1
        [1] [nodemon] to restart at any time, enter `rs`
        [1] [nodemon] watching: *.*
        [1] [nodemon] starting `node server/index.js`
        [1] BDA server listening on port 3003!
        [0] [HPM] Proxy created: /bda-server  ->  http://localhost:3003/
        [0] Starting the development server...
        [0]
        [0] Compiled successfully!
        [0]
        [0] You can now view bda-app in the browser.
        [0]
        [0]   Local:            http://localhost:3000/
        [0]   On Your Network:  http://192.168.56.1:3000/
        [0]
        [0] Note that the development build is not optimized.
        [0] To create a production build, use npm run build.
        [0]
 
Once the BDC editor is up and running, it will open itself in your default browser at this address:

        http://127.0.0.1:3000

## Specifying the Behavioural Drift Computation model
We can now start specifying our behavioural drift computation model. Doing so is similar to building a finite state machine with sensors information on transitions (inputs) and on states (outputs).

First we start by defining the sensors involved in the behavioural drift computation model we are creating.
- Click on the 'Manage sensors' button to bring up the management dialog. 
- From there select create new in the dropdown, press Next and give it a name.
    - Note: allowed characters are uppercase, lowercases, minus and underscore.

You can create all your sensors during this first step or create new ones when you need it. However you will need to create a sensor using the management dialog before using it in a finite state machine's state or transition.

Then you need to specify the finite state machine by creating states and transitions.

Create a state by clicking on 'Create State' and clicking on the interface grey part to place it. To edit state properties, click on the state node. From the right side pane you can change the state's name, choose if this state has a loopback transition and attach sensors to it by clicking on "+" button.

![alt text](./images/create_state.png "Create State")

To create a transition between two states, first click on the 'Create transition' button then click on the source state and then click on the target state. To edit transition properties, select transition by clicking on it and change its name and attached sensors.

![alt text](./images/create_transition.png "Create Transition")

Each sensor added to a state or transition has to be configured, this is done by first setting the constraint field to the appropriate value, then clicking the ⚙ (cog) icon to open the parameter entry dialog and inputting the desired parameters.

## Deploying the Behavioural Drift Computation model

To deploy the specified model, just select 'Export as GeneSIS model' in the main menu. Then specify 3 parameters: the GeneSIS address (eg. http://localhost:8880/) , the Node-RED flow ID (eg. 35c61f8d.61dda) and the Docker IP address (eg. 192.168.99.100). 

![alt text](./images/export_to_genesis.png "Deployment model")

This injects a new Docker instance running Node-RED and the BDC flow in the target GeneSIS application. However, the generated flow is not connected to the actual sensors so some assembly is still required.

## Editing the Behavioural Drift Computation model

You must edit the Node-RED flow to connect the real sensor data and publish the Behavioural Drift value. You will need to access the Node-RED instance the flow is running on available at:

        http://<docker-ip>:1880/

In our example, the instance will be available at:

        http://192.168.99.100:1880/

You will be presented with a Node-RED flow looking similar to this:

![uninit flow](./images/nrflow0.png "Flow")

Connect the sensor data and publish the behavioural drift value using the protocol of your choice. In this example, we use MQTT to subscribe to sensors' values and publish the behavioural drift value.

![init flow](./images/nrflow1.png "Flow")

Deploy the flow and behavioural drift computations will begin.