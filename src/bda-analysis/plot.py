import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


df = pd.read_csv("all_data_o.csv", sep=';', skiprows=[0], names=['Mic1_ZCR','Mic_1_MFCC','size','color'])

# Add index column
index = list(range(0, len(df)))

df['Index'] = pd.DataFrame(index)

# Add alpha channel in order not to plot clustering outliers
df['color'] = np.where(df['color'] == '#ff0000', df['color'] + '00', df['color'] + 'ff')

fig, axes = plt.subplots(nrows=2, ncols=1)

df.plot(ax=axes[0], x ='Index', y='Mic1_ZCR',   kind = 'line', c="black", alpha=0.25)
df.plot(ax=axes[1], x ='Index', y='Mic_1_MFCC', kind = 'line', c="black", alpha=0.25)

df.plot(ax=axes[0], x ='Index', y='Mic1_ZCR',   kind = 'scatter', c='color')
df.plot(ax=axes[1], x ='Index', y='Mic_1_MFCC', kind = 'scatter', c='color')

plt.show()