import pandas
import pydot
import graphviz 
import re
import numpy
import scipy
import abc
import seaborn # Only used for color scheme in BehaviouralDriftAnalyser.__GetTable(). To be removed
import multiprocessing
import time
import argparse

from flexible_clustering import FISHDBC
from sklearn 			 import metrics

#  ____ _           _                             _         _                  _          _               
# / ___| |_   _ ___| |_ ___ _ __ ___ _ __    __ _| |__  ___| |_ _ __ __ _  ___| |_    ___| | __ _ ___ ___ 
#| |   | | | | / __| __/ _ \ '__/ _ \ '__|  / _` | '_ \/ __| __| '__/ _` |/ __| __|  / __| |/ _` / __/ __|
#| |___| | |_| \__ \ ||  __/ | |  __/ |    | (_| | |_) \__ \ |_| | | (_| | (__| |_  | (__| | (_| \__ \__ \
# \____|_|\__,_|___/\__\___|_|  \___|_|     \__,_|_.__/|___/\__|_|  \__,_|\___|\__|  \___|_|\__,_|___/___/ 
                                         
#===================================
class ClusteringEngine(metaclass=abc.ABCMeta):
#===================================

	@classmethod
	def __subclasshook__(cls, subclass):
		return (hasattr(subclass, '__init__') and
				callable(subclass.__init__) and
				hasattr(subclass, 'AddData') and
				callable(subclass.AddData) and
				hasattr(subclass, 'GetClusters') and
				callable(subclass.GetClusters) or
				NotImplemented)

	#iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
	class ClusterData:
	#iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
		id                  = -1
		raw_observations    = None
		membership_scores   = None
		distribution_type   = ''
		distributions       = None

	#-------------------------------------------
	def __GetClustersDataStructure__(self, nb_elements):
	#-------------------------------------------
		i = 0
    	
		clusters = []

		for i in range(nb_elements): # Data structure holding cluster information
			c = self.ClusterData()
			clusters.append(c)
			clusters[i].id = i
			clusters[i].raw_observations = []
			clusters[i].membership_scores = []
			clusters[i].distributions = []
			i += 1

		return clusters

	#-------------------------------------------
	def GetClustersInfo(self, distribution_type):
	#-------------------------------------------

		clusters = []
		# Get all the data from the clustering engine
		labels, membership_scores, raw_data = self.GetClusters()

		clusters = self.__GetClustersDataStructure__(max(labels)+1)

		# Update data structure with membership scores and raw data from which the 
		# distributions will be calculated for each cluster
		for i in range(len(labels)):
			if labels[i] >= 0: # Remove outliers associated to cluster -1
				clusters[labels[i]].raw_observations.append(raw_data[i])
				clusters[labels[i]].membership_scores.append(membership_scores[i])

		# Fit observations to distributions
		distribution = getattr(scipy.stats, distribution_type)
		
		if(max(labels) >= 0): # some clusters have been found
			for i in range(max(labels)+1): # For each cluster

				dim = 1

				if numpy.asarray(clusters[i].raw_observations).ndim > 1:
					dim = clusters[i].raw_observations[0].shape[0]

				if(dim > 1):
					for j in range(dim): # Get data for each observation by column
						data  = [row[j] for row in clusters[i].raw_observations]
						param = distribution.fit(data)
						clusters[i].distributions.append(param)
				else:
						data  = clusters[i].raw_observations
						param = distribution.fit(data)
						clusters[i].distributions.append(param)
				
		return labels, clusters

	@abc.abstractmethod
	def __init__(self):
		raise NotImplementedError

	@abc.abstractmethod
	def AddData(self):
		raise NotImplementedError

	@abc.abstractmethod
	def GetClusters(self):
		raise NotImplementedError

#  ____ _           _                           _             _             _           
# / ___| |_   _ ___| |_ ___ _ __ ___ _ __   ___| |_ _ __ __ _| |_ ___  __ _(_) ___  ___ 
#| |   | | | | / __| __/ _ \ '__/ _ \ '__| / __| __| '__/ _` | __/ _ \/ _` | |/ _ \/ __|
#| |___| | |_| \__ \ ||  __/ | |  __/ |    \__ \ |_| | | (_| | ||  __/ (_| | |  __/\__ \
# \____|_|\__,_|___/\__\___|_|  \___|_|    |___/\__|_|  \__,_|\__\___|\__, |_|\___||___/ 
#                                                                     |___/             

####################################
class FISHDBCStrategy(ClusteringEngine):
####################################

	#------------------------
	def __init__(self, distance_function, min_samples):
	#------------------------
		self.engine = FISHDBC(distance_function, min_samples, m=5, ef=30, m0=16, level_mult=None, heuristic=True, balanced_add=False, vectorized=False)

	#------------------------
	def AddData(self, data):
	#------------------------
		self.engine.add(data)

	#------------------------
	def GetClusters(self):
	#------------------------
		labels, membership_scores, stabilities, condensed_tree, slt, mst = self.engine.cluster()
		return labels, membership_scores, self.engine.data

# _   _      _                    __                  _   _                 
#| | | | ___| |_ __   ___ _ __   / _|_   _ _ __   ___| |_(_) ___  _ __  ___ 
#| |_| |/ _ \ | '_ \ / _ \ '__| | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#|  _  |  __/ | |_) |  __/ |    |  _| |_| | | | | (__| |_| | (_) | | | \__ \
#|_| |_|\___|_| .__/ \___|_|    |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#             |_|                                                           

####################################
class DataManipulation:
####################################
	__instance = None
	@staticmethod 
	def getInstance():
		if DataManipulation.__instance == None:
			Singleton()
		return DataManipulation.__instance
   
	def __init__(self):
		if DataManipulation.__instance != None:
			raise Exception("This class is a singleton!")
		else:
			DataManipulation.__instance = self

	#---------------------------------------------------------
	def ExtractColumnsFromCsv(csv_file, delimiter, col_list, remove_first_row):
	#---------------------------------------------------------
	# see https://www.kite.com/python/answers/how-to-read-specific-column-from-csv-file-in-python

		assert len(col_list) > 0, '[ERROR] The list of sensor names is empty!' 

		df = pandas.read_csv(csv_file, delimiter=delimiter, usecols=col_list)

		assert df.shape[1] == len(col_list), '[ERROR] Not all the required sensors have been found in the csv file!'

		# reorder column to match with col_list order
		df = df[col_list]
		
		df = df.iloc[1:] # remove first row as it contains sensor names
		return df # Then, print(df["col_name"])


	#---------------------------------------------------------
	def GetLabelsFromDot(dot_file):
	#---------------------------------------------------------

		graphs = pydot.graph_from_dot_file(dot_file)
		(g,) = graphs
       
		edge_labels=[]
		node_labels=[]
       
		for edge in g.get_edges():     
			attr = edge.get_attributes()['label']
		
			for m in re.finditer('\w(.+?)->', attr):
				edge_labels.append(attr[m.start():m.end()-2]) if attr[m.start():m.end()-2] not in edge_labels else edge_labels

		for node in g.get_nodes():     
			attr = node.get_attributes()['label']
		
			for m in re.finditer('\w(.+?)->', attr):
				node_labels.append(attr[m.start():m.end()-2]) if attr[m.start():m.end()-2] not in node_labels else node_labels
		return edge_labels, node_labels


####################################
class DistanceFunctions:
####################################
	__instance = None
	@staticmethod 
	def getInstance():
		if DistanceFunctions.__instance == None:
			Singleton()
		return DistanceFunctions.__instance
   
	def __init__(self):
		if DistanceFunctions.__instance != None:
			raise Exception("This class is a singleton!")
		else:
			DistanceFunctions.__instance = self

	#-------------------------------------------
	def Euclidean_distance(x,y):
	#-------------------------------------------
	    return numpy.linalg.norm(x - y)


# ____  ____    _                         
#| __ )|  _ \  / \      ___ ___  _ __ ___ 
#|  _ \| | | |/ _ \    / __/ _ \| '__/ _ \
#| |_) | |_| / ___ \  | (_| (_) | | |  __/
#|____/|____/_/   \_\  \___\___/|_|  \___|
                                         
####################################
class BehaviouralDriftAnalyser:
####################################
	#-------------------------------------------
	def __init__(self, dot_model, min_samples_i, min_samples_o, input_sensor_names=None, output_sensor_names=None):
	#-------------------------------------------
		
		assert min_samples_i >= 5 and min_samples_o >= 5 , 'The min_samples parameters must be greater than or equal to 5!'

		if input_sensor_names is None and output_sensor_names is None:
			self.input_sensor_names, self.output_sensor_names = DataManipulation.GetLabelsFromDot(dot_model)

		# Init clustering engines
		self.clusterer_i = FISHDBCStrategy(DistanceFunctions.Euclidean_distance, min_samples_i)
		self.clusterer_o = FISHDBCStrategy(DistanceFunctions.Euclidean_distance, min_samples_o)

		self.legitimate_data_start = -1000

	#-------------------------------------------
	def SetLegitimateBehaviour(self, legitimate_behaviour_csv, delimiter, distribution_type, data_i=None, data_o=None):
	#-------------------------------------------
		nb_data = self.UpdateModel(legitimate_behaviour_csv, delimiter, "norm", data_i, data_o)
		self.legitimate_data_start = len(self.labels_o) - nb_data
		self.legitimate_data_stop  = len(self.labels_o)

		return nb_data

	#-------------------------------------------
	def UpdateModel(self, data_csv, delimiter, distribution_type, data_i=None, data_o=None, compute_model=True):
	#-------------------------------------------

		if data_i is None and data_o is None:
			data_i = numpy.asarray(DataManipulation.ExtractColumnsFromCsv(data_csv, delimiter, self.input_sensor_names, True))
			data_o = numpy.asarray(DataManipulation.ExtractColumnsFromCsv(data_csv, delimiter, self.output_sensor_names, True))

		#assert len(data_i) == len(data_o), '[ERROR] Number of input observations is different than the number of output observations!'

		for i in data_i:
			self.clusterer_i.AddData(i)
		for o in data_o:
			self.clusterer_o.AddData(o)

		# Get clusters data (raw observations, membership scores, distributions)
		self.labels_i, self.clusters_i = self.clusterer_i.GetClustersInfo(distribution_type)
		self.labels_o, self.clusters_o = self.clusterer_o.GetClustersInfo(distribution_type)

		if compute_model == True:
			self.emission_matrix, self.emission_w_matrix, self.transition_matrix, self.transition_w_matrix = self.__GetModel__(0, len(self.labels_o))

		return len(data_i)

	#-------------------------------------------
	def __GetModel__(self, start_index, stop_index):
	#-------------------------------------------

		assert stop_index  <= len(self.labels_o), 	'[ERROR] Index out of range'
		assert start_index >= 0, 					'[ERROR] start_index must be greater than or equal to 0'
		assert start_index < stop_index, 			'[ERROR] Index start_index > stop_index'

		transition_matrix = []
		emission_matrix = []
		transition_w_matrix = [] # contains state transition weights
		emission_w_matrix = [] # contains state emission weights

		# Init data structures
		if len(self.clusters_o) > 0: # some clusters have been found
			transition_matrix   = [[[None for k in range(len(self.clusters_i))] for i in range(len(self.clusters_o))] for j in range(len(self.clusters_o))]
			transition_w_matrix = [[[0    for k in range(len(self.clusters_i))] for i in range(len(self.clusters_o))] for j in range(len(self.clusters_o))]
			emission_w_matrix   =   [0    for k in range(len(self.clusters_o))]

			for i in range(start_index, stop_index):
				if i > start_index and self.labels_i[i-1] >= 0 and self.labels_o[i-1] >= 0 and self.labels_o[i] >= 0:

					# Update the frequency of occurrence of states and state transitions
					emission_w_matrix[self.labels_o[i]] += 1
					transition_w_matrix[self.labels_o[i-1]][self.labels_o[i]][self.labels_i[i-1]] += 1

                	# Populate elements of the state transition matrix
					if transition_matrix[self.labels_o[i-1]][self.labels_o[i]][self.labels_i[i-1]] == None:
						transition_matrix[self.labels_o[i-1]][self.labels_o[i]][self.labels_i[i-1]] = self.clusters_i[self.labels_i[i-1]].distributions

            	# Populate elements of the emission matrix
				if self.labels_o[i] >= 0:
					emission_matrix.append(self.labels_o[i]) if self.labels_o[i] not in emission_matrix else emission_matrix

			# Normalise transition_w_matrix and emission_w_matrix matrices
			for j in range(len(transition_w_matrix)):
				emission_w_matrix[j] = emission_w_matrix[j]/i
				for k in range(len(transition_w_matrix[j])):
					for l in range(len(transition_w_matrix[j][k])):
						if transition_w_matrix[j][k][l] != 0:
							transition_w_matrix[j][k][l] = transition_w_matrix[j][k][l]/i


		return emission_matrix, emission_w_matrix, transition_matrix, transition_w_matrix

	#-------------------------------------------
	def GetDissimilarityGraph(self, weight_factor):
	#-------------------------------------------
		assert self.legitimate_data_start > -1000, 'No legitimate data available'

		# Get model of the legitimate behaviour
		leg_emission_matrix, leg_emission_w_matrix, leg_transition_matrix, leg_transition_w_matrix = self.__GetModel__(self.legitimate_data_start, self.legitimate_data_stop)

		dot = graphviz.Digraph(comment='DissimilarityGraph')

		# Set states as nodes
		for i in range(len(self.emission_matrix)):
			
			node_color=''
			if self.emission_matrix[i] in leg_emission_matrix:
				node_color='aquamarine4'
			else:
				node_color='red'	

			label = 'State#' + str(self.emission_matrix[i]) + "\n"
			sensor_name_index = 0
			for distribution in self.clusters_o[self.emission_matrix[i]].distributions:
				r = ['%.3f' % round(parms,3) for parms in distribution]
				label += " " + self.output_sensor_names[sensor_name_index] + "->" + str(r) + "\n"
				sensor_name_index+=1
			dot.node(str(self.emission_matrix[i]), label=label, color=node_color, fontcolor=node_color, penwidth=str(max(self.emission_w_matrix[i]*weight_factor,0.5)))

		# Set state transitions as edges
		for i in range(len(self.transition_matrix)): 		    # rows
			for j in range(len(self.transition_matrix[0])): 	# cols
				
				k = 0				
				for distributions in self.transition_matrix[i][j]:	
					if distributions != None:
						
						edge_color = ''
						if distributions in leg_transition_matrix[i][j]:
							edge_color = 'aquamarine4'
						else:
							edge_color = 'red'

						label = "\n"
						sensor_name_index = 0
						for distribution in distributions:
							r = ['%.3f' % round(parms,3) for parms in distribution]
							label += " " + self.input_sensor_names[sensor_name_index] + "->" + str(r) + "\n"
							sensor_name_index+=1							
						dot.edge(str(i), str(j), label=label, color=edge_color, fontcolor=edge_color, penwidth=str(max(self.transition_w_matrix[i][j][k]*weight_factor,0.5)))
					k = k + 1		
		return dot

	#---------------------------------------------------------
	def __GetTable__(self, clusterer, sensor_names):
	#---------------------------------------------------------

		labels, membership_scores, raw_data = clusterer.GetClusters()

		data 					= numpy.asarray(raw_data)
		point_size 				= [x*100 for x in membership_scores]
		cluster_members_size 	= [x if x > 0 else 50 for x in point_size]
		color_palette 			= seaborn.color_palette('Paired', max(labels)+1).as_hex()
		cluster_colors 			= numpy.empty(len(labels), dtype='O')

		for i in range(len(labels)):
			if labels[i] >= 0: # outliers are colored in red
				cluster_colors[i] = color_palette[labels[i]]
			else: # outliers
				cluster_colors[i] = '#ff0000'

		header = numpy.append(sensor_names,"size")
		header = numpy.append(header,"color")

		a = numpy.column_stack((data,cluster_members_size,cluster_colors))
	    
		return numpy.row_stack((header,a))

	#---------------------------------------------------------
	def ExportToCsv(self, prefix, delimiter):
	#---------------------------------------------------------

		table_i = self.__GetTable__(self.clusterer_i, self.input_sensor_names)
		numpy.savetxt(prefix + "_i.csv", table_i, delimiter=delimiter, fmt='%s')
		table_o = self.__GetTable__(self.clusterer_o, self.output_sensor_names)
		numpy.savetxt(prefix + "_o.csv", table_o, delimiter=delimiter, fmt='%s')

#  ____ _           _            _                              _        _          
# / ___| |_   _ ___| |_ ___ _ __(_)_ __   __ _   _ __ ___   ___| |_ _ __(_) ___ ___ 
#| |   | | | | / __| __/ _ \ '__| | '_ \ / _` | | '_ ` _ \ / _ \ __| '__| |/ __/ __|
#| |___| | |_| \__ \ ||  __/ |  | | | | | (_| | | | | | | |  __/ |_| |  | | (__\__ \
# \____|_|\__,_|___/\__\___|_|  |_|_| |_|\__, | |_| |_| |_|\___|\__|_|  |_|\___|___/
#                                        |___/ 
#       _         _                  _          _               
#  __ _| |__  ___| |_ _ __ __ _  ___| |_    ___| | __ _ ___ ___ 
# / _` | '_ \/ __| __| '__/ _` |/ __| __|  / __| |/ _` / __/ __|
#| (_| | |_) \__ \ |_| | | (_| | (__| |_  | (__| | (_| \__ \__ \
# \__,_|_.__/|___/\__|_|  \__,_|\___|\__|  \___|_|\__,_|___/___/ 

#===================================
class MinSamplesOptimizer(metaclass=abc.ABCMeta):
#===================================

	@classmethod
	def __subclasshook__(cls, subclass):
		return (hasattr(subclass, '__GetScore__') and
				callable(subclass.__GetScore__) or
				NotImplemented)

	#--------------------------------------
	def __init__(self, dot_model, csv_data, delimiter):
	#--------------------------------------

		self.input_sensor_names, self.output_sensor_names = DataManipulation.GetLabelsFromDot(dot_model)
		self.data_i = numpy.asarray(DataManipulation.ExtractColumnsFromCsv(csv_data, delimiter, self.input_sensor_names, True))
		self.data_o = numpy.asarray(DataManipulation.ExtractColumnsFromCsv(csv_data, delimiter, self.output_sensor_names, True))

		self.nb_obs = len(self.data_i)

		assert self.nb_obs == len(self.data_o), '[ERROR] Inconsistent number of input/output observations'

	#--------------------------------------
	def __GetScores__(self, min_samples):
	#--------------------------------------

		x = BehaviouralDriftAnalyser("", min_samples, min_samples, self.input_sensor_names, self.output_sensor_names)
		nb_data = x.UpdateModel("", ';', "norm", self.data_i, self.data_o, False)

		outlier_indexes = numpy.where(x.labels_i == -1)[0]

		score_i = self.__GetScore__(numpy.delete(self.data_i, outlier_indexes, 0), numpy.delete(x.labels_i, outlier_indexes, 0))
		score_o = self.__GetScore__(numpy.delete(self.data_o, outlier_indexes, 0), numpy.delete(x.labels_i, outlier_indexes, 0))

		return (min_samples, score_i, score_o)

	#--------------------------------------
	def Execute(self): 
	#--------------------------------------

		self.results = []

		time_start = time.time()

		with multiprocessing.Pool(multiprocessing.cpu_count()) as pool:
			#self.results.append(pool.starmap(self.__GetScores__, [((min_samples,)) for min_samples in range(self.nb_obs//8,self.nb_obs//4,int(0.05*(self.nb_obs//4-self.nb_obs//8)))]))
			self.results.append(pool.starmap(self.__GetScores__, [((min_samples,)) for min_samples in range(95,140,1)]))
		pool.close()

		time_stop = time.time()
		print("Elapsed time : " + str(time_stop-time_start))

		min_samples_i = max(self.results[0],key=lambda item:item[1])[0]
		min_samples_o = max(self.results[0],key=lambda item:item[2])[0]	

		return min_samples_i, min_samples_o


	@abc.abstractmethod
	def __GetScore__(self):
		raise NotImplementedError

# __  __      _        _                _             _             _           
#|  \/  | ___| |_ _ __(_) ___ ___   ___| |_ _ __ __ _| |_ ___  __ _(_) ___  ___ 
#| |\/| |/ _ \ __| '__| |/ __/ __| / __| __| '__/ _` | __/ _ \/ _` | |/ _ \/ __|
#| |  | |  __/ |_| |  | | (__\__ \ \__ \ |_| | | (_| | ||  __/ (_| | |  __/\__ \
#|_|  |_|\___|\__|_|  |_|\___|___/ |___/\__|_|  \__,_|\__\___|\__, |_|\___||___/
#                                                             |___/             
#===================================
class SilhouetteStrategy(MinSamplesOptimizer):
#===================================
# If the ground truth labels are not known, evaluation must be performed using the model itself. 
# The Silhouette Coefficient (sklearn.metrics.silhouette_score) is an example of such an evaluation, 
# where a higher Silhouette Coefficient score relates to a model with better defined clusters. 
# The Silhouette Coefficient is defined for each sample and is composed of two scores:

#	a: The mean distance between a sample and all other points in the same class.
#	b: The mean distance between a sample and all other points in the next nearest cluster.

# The score is bounded between -1 for incorrect clustering and +1 for highly dense clustering. 
# Scores around zero indicate overlapping clusters.
# The score is higher when clusters are dense and well separated, which relates to a standard 
# concept of a cluster.

	#-------------------------------------------
	def __GetScore__(self, data, labels):
	#-------------------------------------------
		score = -1
		if len(labels)>0:
			if max(labels) >=1: # One needs a minimum of two clusters for the metric to work
				score = metrics.silhouette_score(data, labels)
		return score

#===================================
class CalinskiHarabaszStrategy(MinSamplesOptimizer):
#===================================
# If the ground truth labels are not known, the Calinski-Harabasz index 
# (sklearn.metrics.calinski_harabasz_score) - also known as the Variance Ratio Criterion - can be 
# used to evaluate the model, where a higher Calinski-Harabasz score relates to a model with better 
# defined clusters.

# The score is higher when clusters are dense and well separated, which relates to a standard concept of a cluster.
# The score is fast to compute

	#-------------------------------------------
	def __GetScore__(self, data, labels):
	#-------------------------------------------
		score = -1
		if len(labels)>0:
			if max(labels) >=1: # One needs a minimum of two clusters for the metric to work
				score = metrics.calinski_harabasz_score(data, labels)
		return score

#===================================
class DaviesBouldinStrategy(MinSamplesOptimizer):
#===================================
# If the ground truth labels are not known, the Davies-Bouldin index (sklearn.metrics.davies_bouldin_score) 
# can be used to evaluate the model, where a lower Davies-Bouldin index relates to a model with better separation 
# between the clusters.

# This index signifies the average ‘similarity’ between clusters, where the similarity is a measure that compares 
# the distance between clusters with the size of the clusters themselves.

# Zero is the lowest possible score. Values closer to zero indicate a better partition.

	#-------------------------------------------
	def __GetScore__(self, data, labels):
	#-------------------------------------------
		score = -1
		if len(labels)>0:
			if max(labels) >=1: # One needs a minimum of two clusters for the metric to work
				score = 1 - metrics.davies_bouldin_score(data, labels)
		return score



# __  __       _       
#|  \/  | __ _(_)_ __  
#| |\/| |/ _` | | '_ \ 
#| |  | | (_| | | | | |
#|_|  |_|\__,_|_|_| |_|
                      
if __name__ == '__main__':

	parser = argparse.ArgumentParser(description="Behavioural drift analysis toolkit.")
	parser.add_argument("-d", "--dot", 			 required=True, type=str, help="dot file from which are extracted inputs and output sensor names.")
	parser.add_argument("-e", "--exp", 			 required=True, type=str, help="csv file containing the expected behaviour")
	parser.add_argument("-u", "--unexp", 		 required=True, type=str, help="csv file containing behaviour observed in the field")
	parser.add_argument("-i", "--min_samples_i", required=True, type=int, help="HDBSCAN min_samples parameter value for input observation space clustering")
	parser.add_argument("-o", "--min_samples_o", required=True, type=int, help="HDBSCAN min_samples parameter value for output observation space clustering")
	parser.add_argument("-s", "--delimiter", 	 required=True, type=str, help="csv columun delimiter")
	args = parser.parse_args()

	x = BehaviouralDriftAnalyser(args.dot, args.min_samples_i, args.min_samples_o)
	x.SetLegitimateBehaviour(args.exp, args.delimiter, "norm")
	x.UpdateModel(args.unexp, args.delimiter, "norm")

	graph = x.GetDissimilarityGraph(25)

	graph.save('./dissimilarity_graph'+'.dot')
	x.ExportToCsv('all_data', ';')
