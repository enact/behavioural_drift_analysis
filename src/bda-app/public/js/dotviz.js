let graphviz = null;

let saveddot = null;
let inputs = [];
let outputs = [];
let parseddot = null;
window.dotfiles = [];

function renderDot(dot) {
	saveddot = dot;
	// render the graph
    graphviz = d3.select("#analysisdot").graphviz({ engine: engines[engineIndex], scale: 0.9 })/*.transition(function () {
        return d3.transition("main")
            .ease(d3.easeLinear)
            .duration(200);
    })*/.fit(false).logEvents(false).attributer(attributer).on("initEnd", () => {
        graphviz.renderDot(dot, renderDotEnd);
	});

	// parse the dot and extract inputs and outputs
	parseddot = dotparse(dot)[0];
	inputs = [];
	outputs = [];
	parseddot.children.forEach(child => {
		if (child.type === "edge_stmt") {
			child.attr_list.forEach(attr => {
				if (attr.id === "label") {
					try {
						attr.eq.split("\n").forEach(line => {
							let linesplit = line.split("->");
							if (linesplit.length === 1) return;
							outputs.push(linesplit[0].trim());
						});
					} catch (e) {
						console.log(e);
						console.log(JSON.parse(JSON.stringify(attr)));
                    }
                }
			});
		}
		if (child.type === "node_stmt") {
			child.attr_list.forEach(attr => {
				if (attr.id === "label") {
					try {
						attr.eq.split("\n").forEach(line => {
							let linesplit = line.split("->");
							if (linesplit.length === 1) return;
							inputs.push(linesplit[0].trim());
						});
					} catch (e) {
						console.log(e);
						console.log(JSON.parse(JSON.stringify(attr)));
					}
				}
			});
        }
	});
	inputs = [...new Set(inputs)];
	outputs = [...new Set(outputs)];
}
window.renderDot = renderDot;

//https://github.com/magjac/d3-graphviz/issues/7#issuecomment-348393896
function renderDotEnd() {
	/* Remove width and height attributes from svg element,
	   so that it scales to fit its container */
	d3.select("#analysisdot > svg")
		.attr("width", "100%")
		.attr("height", "100%")
}

// the attributer is used to remove background color
function attributer(datum, index, nodes) {
    if (datum.tag == "polygon" && datum.parent.attributes.id == 'graph0') {
        datum.attributes.fill = "transparent";
    }
    if (datum.tag == "text") {
        datum.attributes["font-family"] = "sans-serif";
        datum.attributes["font-size"] = "11";
    }
}

const engines = ["circo", "dot", "fdp", "neato", "osage", "patchwork", "twopi"];
let engineIndex = 1;
function changeDotEngine() {
    engineIndex = (engineIndex + 1) % engines.length;
    renderDot(saveddot);
}

function getColorsForLabel(isInput, label) {
	var color = Chart.helpers.color;
	if (isInput) {
		return [color(window.chartColors.green).alpha(0.5).rgbString(), window.chartColors.green];
	} else {
		return [color(window.chartColors.red).alpha(0.5).rgbString(), window.chartColors.red];
    }
}


function renderGraph(obs, title, canvasid, drawinputs = true, drawoutputs = true, heightMult = 0.40, blackline=false) {
	const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
	const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

	let width = 0.49 * vw;
	let height = heightMult * vh;

	console.log("RenderGraph " + canvasid + " " + width + "x" + height);

	// clear
	d3.select("#" + canvasid).selectAll("*").remove();

	let psv = d3.dsvFormat(";");
	let parseddata = psv.parse(obs, d3.autoType).map((val, idx) => {
		let ret = { date: idx };
		ret.maxvalue = 0;
		if (drawinputs) {
			inputs.forEach(input => {
				ret[input] = val[input];
				if (val[input] > ret.maxvalue) {
					ret.maxvalue = val[input];
				}
			});
		}
		if (drawoutputs) {
			outputs.forEach(output => {
				ret[output] = val[output];
				if (val[output] > ret.maxvalue) {
					ret.maxvalue = val[output];
				}
			});
		}

		// extra params seen in the outputs from analysis
		ret.color = val.color;
		ret.size = val.size;
		return ret;
	});
	let data = Object.assign(parseddata);
	console.log(data);


	let margin = ({ top: 20, right: 20, bottom: 30, left: 30 });

	let y = d3.scaleLinear()
		.domain([0, d3.max(data, d => d.maxvalue)])/*.nice()*/
		.range([height - margin.bottom, margin.top]);

	let x = d3.scaleLinear()
		.domain([0, d3.max(data, d => d.date)])/*.nice()*/
		.range([margin.left, width - margin.right]);

	let yAxis = g => g
		.attr("transform", `translate(${margin.left},0)`)
		.call(d3.axisLeft(y)
			.tickValues(d3.scaleLinear().domain(y.domain()).ticks()))
		.call(g => g.selectAll(".tick line").clone()
			.attr("stroke-opacity", 0.2)
			.attr("x2", width - margin.left - margin.right))
		.call(g => g.select(".domain").remove());

	let xAxis = (g, x) => g
		.attr("transform", `translate(0,${height - margin.bottom})`)
		.call(d3.axisBottom(x).ticks(width / 80).tickSizeOuter(0));

	let chart = function () {
		let zoom = d3.zoom()
			.scaleExtent([1, 8])
			.extent([[margin.left, 0], [width - margin.right, height]])
			.translateExtent([[margin.left, -Infinity], [width - margin.right, Infinity]])
			.on("zoom", zoomed);

		let svg = d3.create("svg")
			.attr("viewBox", [0, 0, width, height]);

		let clip = svg.append("defs").append("svg:clipPath")
			.attr("id", "clip")
			.append("svg:rect")
			.attr("width", width)
			.attr("height", height)
			.attr("x", 0)
			.attr("y", 0);

		let g = svg.append("g")
			.attr("stroke-linecap", "round")
			.attr("stroke", "black")
			.selectAll("g")
			.data(data)
			.join("g")
			.attr("transform", d => `translate(${x(d.date)},0)`)
			.attr("clip-path", "url(#clip)");

		let paths = []; 
		let incolors = ["#ff00ff", "#00ffff"];
		let outcolors = ["#ff0000", "#0000ff"];
		let legendIdx = -1;
		if (drawinputs) {
			inputs.forEach((label, idx) => {
				let line = d3.line()
					.defined(d => !isNaN(d[label]))
					.x(d => x(d.date))
					.y(d => y(d[label]));

				let path = svg.append("path")
					.datum(data)
					.attr("fill", "none")
					.attr("stroke", blackline ? "#000000" : incolors[idx])
					.attr("stroke-width", 0.5)
					.attr("stroke-linejoin", "round")
					.attr("stroke-linecap", "round")
					.attr("d", line);

				let dots = svg.append("g")
					.attr("stroke-width", 1.5)
					.attr("fill", "none")
					.selectAll("circle")
					.data(data.filter(d => !isNaN(d[label])))
					.join("circle")
					.attr("cx", d => x(d.date))
					.attr("cy", d => y(d[label]))
					.attr("stroke", d => d.color || incolors[idx])
					.attr("r", 1);

				paths.push({ path, line, dots, label, x, y });

				let lineLegend = svg.append("g")
					.attr("class", "lineLegend")
					.attr("transform", function (d, i) {
						legendIdx++;
						return "translate(" + (margin.left + 10) + "," + (legendIdx * 20) + ")";
					});

				lineLegend.append("text").text(label)
					.attr("font-size", 10)
					.attr("transform", "translate(15,9)"); //align texts with boxes

				lineLegend.append("rect")
					.attr("fill", function (d, i) { return incolors[idx]; })
					.attr("width", 10).attr("height", 10);
			});
		}
		if (drawoutputs) {
			outputs.forEach((label, idx) => {
				let line = d3.line()
					.defined(d => !isNaN(d[label]))
					.x(d => x(d.date))
					.y(d => y(d[label]));

				let path = svg.append("path")
					.datum(data)
					.attr("fill", "none")
					.attr("stroke", blackline ? "#000000" : outcolors[idx])
					.attr("stroke-width", 1.5)
					.attr("stroke-linejoin", "round")
					.attr("stroke-linecap", "round");
				path.attr("d", line);

				let dots = svg.append("g")
					.attr("stroke-width", 1.5)
					.attr("fill", "none")
					.selectAll("circle")
					.data(data.filter(d => !isNaN(d[label])))
					.join("circle")
					.attr("cx", d => x(d.date))
					.attr("cy", d => y(d[label]))
					.attr("stroke", d => d.color || outcolors[idx])
					.attr("r", 1);

				paths.push({ path, line, dots, label,x ,y });

				let lineLegend = svg.append("g")
					.attr("class", "lineLegend")
					.attr("transform", function (d, i) {
						legendIdx++;
						return "translate(" + (margin.left + 10) + "," + (legendIdx * 20) + ")";
					});

				lineLegend.append("text").text(label)
					.attr("font-size", 10)
					.attr("transform", "translate(15,9)"); //align texts with boxes

				lineLegend.append("rect")
					.attr("fill", function (d, i) { return outcolors[idx]; })
					.attr("width", 10).attr("height", 10);
			});
		}

		let gx = svg.append("g")
			.call(xAxis, x);

		svg.append("g")
			.call(yAxis, y);

		svg.call(zoom)
			.transition()
			.duration(750);

		function zoomed() {
			let event = d3.event;
			if(!event) return
			let xz = event.transform.rescaleX(x);
			gx.call(xAxis, xz);
			paths.forEach(drawnthing => {
				let line = d3.line()
					.defined(d => !isNaN(d[drawnthing.label]))
					.x(d => xz(d.date))
					.y(d => drawnthing.y(d[drawnthing.label]));


				drawnthing.path.attr("d", line);
				drawnthing.dots.attr("cx", function (d) {
					return xz(d.date);
				});

			});
		}

		return svg.node();
	}();

	document.getElementById(canvasid).appendChild(chart);
}

function renderGraphOld(obs, title, canvasid){
	const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
	const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

	let width = vw;
	let height = 0.4 * vh;

	// gotta use a dsv psv whatever because they're not csv they're ssv semicolons things yay
	let psv = d3.dsvFormat(";");
	let parseddata = psv.parse(obs, d3.autoType).map((val, idx) => {
		let ret = { date: idx };
		inputs.forEach(input => ret[input] = val[input]);
		outputs.forEach(output => ret[output] = val[output]);
		return ret;
	});
	let data = Object.assign(parseddata);
	console.log(data);

	let labels = [], datasets = [];
	inputs.forEach(input => {
		let colors = getColorsForLabel(true, input);
		datasets.push({
			label: input,
			backgroundColor: colors[0],
			borderColor: colors[1],
			fill: false,
			data: []
		});
	})
	outputs.forEach(output => {
		let colors = getColorsForLabel(false, output);
		datasets.push({
			label: output,
			backgroundColor: colors[0],
			borderColor: colors[1],
			fill: false,
			data: []
		});
	})
	data.forEach(datapoint => {
		labels.push(datapoint.date);
		let i = 0;
		for (let k in datapoint) {
			if (k === "date") continue;
			datasets[i].data.push(datapoint[k]);
			i++;
		}
	});

	var config = {
		type: 'line',
		data: {
			labels: labels,
			datasets: datasets
		},
		options: {
			responsive: true,
			maintainAspectRatio: true,
			aspectRatio: 6.8, // see if that flies
			title: {
				text: title
			},
			scales: {
				xAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'observation'
					}
				}],
				yAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'data'
					}
				}]
			},
			tooltips: {
				enabled: true
			}
		}
	};

	var ctx = document.getElementById(canvasid).getContext('2d');
	window.myLine = new Chart(ctx, config);
}

function renderObs(obs) {
	renderGraph(obs, "Field", "obscanvas", true, false, 0.20);
	renderGraph(obs, "Field", "obscanvas2", false, true, 0.20);
}

function renderExpected(obs) {
	//renderGraph(obs, "Expected", "expcanvas");
}

function renderAllDataI(data) {
	renderGraph(data, "all_data_i", "all_data_i_canvas", true, true, undefined, true);
}

function renderAllDataO(data) {
	renderGraph(data, "all_data_o", "all_data_o_canvas", true, true, undefined, true);
}