var fsm_ed_mode = {
	NONE: 0,
	CREATE_STATE: 1,
	CREATE_TRANSITION: 2,
	PARAMETRIZE: 3,
};

var fsm_editor = {
	cy: window.cy,
	mode: fsm_ed_mode.NONE,
    model: {
        states: [],
        transitions: []
    },
	parametrizeCallback: null,
	oneShotCreates: true,
	defaultMode: fsm_ed_mode.PARAMETRIZE,

	// init
	init: function (config) {
		this.handleParametrizeCB = this.handleParametrizeCB.bind(this);
		this.loadModel = this.loadModel.bind(this);
        this.exportSCXML = this.exportSCXML.bind(this);
        this.exportJSON = this.exportJSON.bind(this);
        this.onViewport = this.onViewport.bind(this);

        this.model = {
            states: [],
            transitions: []
        };
		this.initCytoscape();
		this.hookEvents();
        this.setDefaultMode();

        if (config) {
            if (config.parametrizeCallback) {
                this.parametrizeCallback = config.parametrizeCallback;
            }

            if (config.startModel && config.startModel.states && config.startModel.transitions) {
                console.log("applying model:");
                console.log(config.startModel);
                this.loadModel(config.startModel);
            }
        }
	},

	// load model, used for start model
    loadModel: function (model) {
        this.init();
        this.model = model;
		var statex = 100;
		for (let stateidx in model.states) {
            let newNode = {
				group: "nodes",
				data: model.states[stateidx],
				position: { x: statex, y: 100 }
			};
			this.cy.add(newNode);
			statex += 300;
		}

        for (let transidx in model.transitions) {
            let newTransition = {
				group: "edges",
				data: model.transitions[transidx]
			};
			this.cy.add(newTransition);
		}
    },

	// change modes
	setCSMode: function () { /*console.log("setCSMode");*/ this.mode = fsm_ed_mode.CREATE_STATE; },
	setCTMode: function () { /*console.log("setCTMode");*/ this.mode = fsm_ed_mode.CREATE_TRANSITION; },
	setPMMode: function () { /*console.log("setPMMode");*/ this.mode = fsm_ed_mode.PARAMETRIZE; },
	setDefaultMode: function () { /*console.log("default");*/ this.mode = this.defaultMode; },

	// export
	exportSCXML: function (xmlModel) {
		this.download("fsm_export.xml", xmlModel);
    },

    exportJSON: function (extraProps) {
        let exportModel = this.model;
        // process extraprops
        if (extraProps) {
            exportModel.extraProps = {};
            extraProps.forEach(p => exportModel.extraProps[p.name] = p.data);
        }
        this.download("fsm_export.json", JSON.stringify(exportModel));
    },
	
	// init cy
    initCytoscape: function () {
        //cleanup first
        if (this.cy) this.cy.destroy();
        if (this.cy_nav) this.cy_nav.destroy();

		this.cy = window.cy = cytoscape({
			container: document.getElementById('fsm_editor'),

			boxSelectionEnabled: false,
			autounselectify: true,

			style: [
				{
					selector: 'node',
					css: {
						'content': 'data(label)',
						'background-fit': 'contain',
						'background-image-opacity': '0.3',
						'text-valign': 'center',
						'text-halign': 'center',
						'font-size': '14px',
						'font-weight': 'bold',
						'width': '60px',
						'height': '60px'
					}
				},
				{
					selector: 'edge',
					css: {
						'curve-style': 'bezier',
						'target-arrow-shape': 'triangle',
						'text-wrap': 'wrap',
						'text-max-width': '150px',
						'content': 'data(label)'
					}
				},
				{
					selector: ':selected',
					css: {
						'background-color': 'black',
						'line-color': 'black',
						'target-arrow-color': 'black',
						'source-arrow-color': 'black'
					}
				}
			],

			elements: {
				nodes: [],
				edges: []
			},

			layout: {
				name: 'preset',
				padding: 5
			}

        });
        console.log("Cytoscape init OK");

        this.cy_nav = this.cy.navigator({
            container: "#cy_nav",
            removeCustomContainer: false
        });
        console.log("Cytoscape Navigator init OK");
	},

	hookEvents: function () {
		var editor_model = this;
		this.cy.on("tap", function (event) {
			switch (editor_model.mode) {
				case fsm_ed_mode.CREATE_STATE:
					editor_model.handleEventCS(event);
					break;
				case fsm_ed_mode.CREATE_TRANSITION:
					editor_model.handleEventCT(event);
					break;
				case fsm_ed_mode.PARAMETRIZE:
					editor_model.handleEventPM(event);
					break;
				default:
					break;
			}
        });
        this.cy.on("viewport", editor_model.onViewport);
    },

    onViewport: function (e) {
        //console.log(this.cy.zoom());
        //console.log(this.cy.pan());
        let zoomFactor = this.cy.zoom();
        let defaultZoom = 2;
        document.getElementById("fsm_editor").style.backgroundSize = defaultZoom * zoomFactor + "vh " + defaultZoom * zoomFactor + "vh";

        let panPos = this.cy.pan();
        document.getElementById("fsm_editor").style.backgroundPosition = panPos.x + "px " + panPos.y + "px";
    },

	handleEventCS: function (event) {
		var id = nodeIDGenerator();
		var newNode = {
			group: "nodes",
			data: {
				id: id, type: "state", label: "state", loopback: false, sensors: []
			},
			position: event.position
		};
		this.cy.add(newNode);
		this.model.states.push(newNode.data);

		if (this.oneShotCreates) {
			this.setDefaultMode();
		}
	},
	createTransBuffer: null,
	handleEventCT: function (event) {
		// skip if it's on the background
		if (event.target === event.cy) return;

		// no buffer if src, buffer if dst
		if (this.createTransBuffer == null) {
			this.createTransBuffer = { srcNode: event.target };
		} else {
			var id = nodeIDGenerator();
			var newTransition = {
				group: "edges",
				data: { id: id, type: "transition", source: this.createTransBuffer.srcNode.id(), target: event.target.id(), sensors:[] }
			};
			this.cy.add(newTransition);
			this.model.transitions.push(newTransition.data);
			this.createTransBuffer = null;

			if (this.oneShotCreates) {
				this.setDefaultMode();
			}
		}
	},
	handleEventPM: function (event) {
		// skip if it's on the background
		if (event.target === event.cy) return;
		this.parametrizeCallback(event.target, this.handleParametrizeCB);

		if (this.oneShotCreates) {
			this.setDefaultMode();
		}
	},

	// callback from the UI after parametrize
    handleParametrizeCB: function (data) {
        if (data.mode === "delete") {
            this.handleDelete(data);
        } else {
            this.handleUpdate(data);
        }
    },

    handleDelete: function (data) {
        let editor = this, editor_model = this.model;
        switch (data.type) {
            case "state":
                editor_model.states = editor_model.states.filter(state => state.id !== data.id);
                editor.cy.$('[id="' + data.id + '"]').forEach(el => el.remove());
                // loopback transition if it exists
                editor.cy.$('[id="lb_' + data.id + '"]').forEach(el => el.remove());
                editor_model.transitions = editor_model.transitions.filter(trans => trans.id !== "lb_" + data.id);
                // other transitions using this state
                let transToDel = editor_model.transitions.filter(trans => trans.source === data.id || trans.target === data.id);
                transToDel.forEach(trans => editor.cy.$('[id="' + trans.id + '"]').forEach(el => el.remove()));
                editor_model.transitions = editor_model.transitions.filter(trans => trans.source !== data.id && trans.target !== data.id);
                break;
            case "transition":
                editor_model.transitions = editor_model.transitions.filter(trans => trans.id !== data.id);
                editor.cy.$('[id="' + data.id + '"]').forEach(edge => edge.remove());
                break;
            default:
                break;
        }
    },

    handleUpdate: function (data) {
        let editor = this, editor_model = this.model;
        switch (data.type) {
            case "state":
                for (var stateidx in editor_model.states) {
                    if (editor_model.states[stateidx].id == data.id) {
                        editor_model.states[stateidx].label = data.label;
                        editor_model.states[stateidx].sensors = data.sensors;

                        // get link if already created
                        let links = editor.cy.$('[id = "lb_' + data.id + '"]');
                        if (data.loopback) {
                            if (links.length === 0) {
                                // add loopback link
                                var newTransition = {
                                    group: "edges",
                                    data: { id: "lb_" + data.id, type: "transition", source: data.id, target: data.id, input: null, output: null, sensors: [] }
                                };
                                editor.cy.add(newTransition);
                                editor_model.transitions.push(newTransition.data);
                            }
                        } else {
                            // delete loopback link
                            links.remove();
                        }

                        editor_model.states[stateidx].loopback = data.loopback;
                    }
                }
                break;
            case "transition":
                for (var transidx in editor_model.transitions) {
                    if (editor_model.transitions[transidx].id == data.id) {
                        editor_model.transitions[transidx].input = data.input;
                        editor_model.transitions[transidx].output = data.output;
                        editor_model.transitions[transidx].sensors = data.sensors;
                        editor_model.transitions[transidx].label = data.label;
                    }
                }
                break;
            default:
                break;
        }
        editor.cy.$('[id = "' + data.id + '"]').flashClass("foo", 1000);
    },

	// download function from https://ourcodeworld.com/articles/read/189/how-to-create-a-file-and-generate-a-download-with-javascript-in-the-browser-without-a-server
	download: function (filename, text) {
		var element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		element.setAttribute('download', filename);

		element.style.display = 'none';
		document.body.appendChild(element);

		element.click();

		document.body.removeChild(element);
	},

	import: function (model) {
		this.loadModel(model);
	}
}

function nodeIDGenerator() {
	return generateID(8) + "." + generateID(6);
}

// generate hex id https://stackoverflow.com/a/27747377
// dec2hex :: Integer -> String
function dec2hex(dec) {
	return ('0' + dec.toString(16)).substr(-2);
}

// generateId :: Integer -> String
function generateID(len) {
	var arr = new Uint8Array((len || 40) / 2);
	window.crypto.getRandomValues(arr);
	return Array.from(arr, dec2hex).join('');
}
// generate hex id https://stackoverflow.com/a/27747377
