import React, { Component } from 'react';
import './App.css';
import "antd/dist/antd.css";
import BDAConfigurator from "./BDAConfigurator.js"; 

class App extends Component {
	render() {
		return (
				<BDAConfigurator/>
		);
	}
}

export default App;
