﻿import FSMConfigurationFields from './FSMConfigurationFields';
import React, { Component } from 'react';
import { Modal, Layout, Menu, Input, Button, Select, Form, Icon, notification, Tabs, message, Upload, Tooltip, Switch, Row, Col, Slider, InputNumber } from 'antd';

const {
	Header, Sider, Content, Footer
} = Layout
const { Option } = Select;
const SubMenu = Menu.SubMenu;
const TabPane = Tabs.TabPane;
const { Dragger } = Upload;

class BDAConfigurator extends Component {
	state = {
		screen: "welcome",
		configurationModalVisible: false,
		configurationModalParameters: null,
		genesisDialogVisible: false,
        importDialogVisible: false,
        sensorMgmtModalVisible: false,
        sensorCfgModalVisible: false,
        initTooltipVisible: true,
        loadDotModalVisible: false,
        setParamModalVisible: false,
        sliderValue: 0,
        maxObservation: 20,
        isDotLoaded: false,
        isObsLoaded: false,
        isExpLoaded: false,
        mode: "configuration",
        sensors: [],
		genesis: {
			dockerip: "127.0.0.1",
			id: "35c61f8d.61dda",
            genesisip: "http://localhost:8880/"
        },
        fileList: [],
        headerPicture: "img/bdac_logo_header.png",
        parameters: { delimiter: ";", MIN_SAMPLES_I: 40, MIN_SAMPLES_O: 40 }
	}

	constructor() {
		super();

		this.initEditor = this.initEditor.bind(this);
		this.validateParametrization = this.validateParametrization.bind(this);
		this.openParametrizeWindow = this.openParametrizeWindow.bind(this);
		this.handleParameterOnChange = this.handleParameterOnChange.bind(this);
        this.showImportModal = this.showImportModal.bind(this);
        this.closeImportModal = this.closeImportModal.bind(this);
        this.importModel = this.importModel.bind(this);
        this.createStateMode = this.createStateMode.bind(this);

        this.openSensorMgmtModal = this.openSensorMgmtModal.bind(this);
        this.updateSensorMgmtTempSensor = this.updateSensorMgmtTempSensor.bind(this);
        this.handleSensorOnChange = this.handleSensorOnChange.bind(this);
        this.validateSensorConfiguration = this.validateSensorConfiguration.bind(this);
        this.cancelSensorConfiguration = this.cancelSensorConfiguration.bind(this);

        this.openConfigurationModal = this.openConfigurationModal.bind(this);
        this.export = this.export.bind(this);
        this.exportJSON = this.exportJSON.bind(this);
        this.exportGenesis = this.exportGenesis.bind(this);
        this.exportNodeRed = this.exportNodeRed.bind(this);
        this.exportNodeRedClipboard = this.exportNodeRedClipboard.bind(this);
        this.exportGenesisDialog = this.exportGenesisDialog.bind(this);
        this.exportGenesisDownload = this.exportGenesisDownload.bind(this);
		this.addSensor = this.addSensor.bind(this);

        this.loadFromFile = this.loadFromFile.bind(this);
		this.handleFileRead = this.handleFileRead.bind(this);

		this.clearPendingParametrization = this.clearPendingParametrization.bind(this);
        this.validateCurveParametrization = this.validateCurveParametrization.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.clearPendingCurveParametrization = this.clearPendingCurveParametrization.bind(this);

        this.handleChangeTab = this.handleChangeTab.bind(this);

        this.changeMode = this.changeMode.bind(this);
        this.showLoadDotModal = this.showLoadDotModal.bind(this);
        this.closeDotModal = this.closeDotModal.bind(this);
        this.loadDot = this.loadDot.bind(this);
        this.showLoadObsModal = this.showLoadObsModal.bind(this);
        this.closeObsModal = this.closeObsModal.bind(this);
        this.loadObs = this.loadObs.bind(this);

        this.showLoadExpectedModal = this.showLoadExpectedModal.bind(this);
        this.closeExpectedModal = this.closeExpectedModal.bind(this);
        this.loadExpected = this.loadExpected.bind(this);

        this.showParametersModal = this.showParametersModal.bind(this);
        this.closeParametersModal = this.closeParametersModal.bind(this);
        this.saveParameters = this.saveParameters.bind(this);

        this.onStepSliderChange = this.onStepSliderChange.bind(this);
        this.startAnalysis = this.startAnalysis.bind(this);

        this.startLoadSpinner = this.startLoadSpinner.bind(this);
        this.clearLoadSpinner = this.clearLoadSpinner.bind(this);

        window.BDAConfigurator = this;
    }

    changeMode(checked) {
        this.setState({
            headerPicture: "img/bda" + (checked ? "" : "c") + "_logo_header.png",
            mode: (checked ? "analysis" : "configuration"),
            initTooltipVisible: false
        })
    }

	initEditor() {
		//var sm = { "states": [{ "id": "68090b14.0bc852", "type": "state", "label": "state", "loopback": false, "sensors": [] }, { "id": "b7854c9f.c6a619", "type": "state", "label": "state", "loopback": true, "sensors": [{ "name": "c", "constraint": "prob", "parameters": { "mean": 0, "sd": 0 } }] }], "transitions": [{ "id": "5ab2535c.830135", "type": "transition", "source": "68090b14.0bc852", "target": "b7854c9f.c6a619", "sensors": [{ "name": "b", "constraint": "", "parameters": { "mean": 0, "sd": 0 } }] }, { "id": "lb_b7854c9f.c6a619", "type": "transition", "source": "b7854c9f.c6a619", "target": "b7854c9f.c6a619", "input": null, "output": null, "sensors": [{ "name": "a", "constraint": "prob", "parameters": { "mean": 0, "sd": 0 } }] }] };
        var config = { parametrizeCallback: this.openParametrizeWindow, /*startModel: sm*/ };
        if (window.fsm_editor) {
            window.fsm_editor.init(config);
        }
	}

    createStateMode() {
        message.info("Click in the grey area to create a new state");
        window.fsm_editor.setCSMode();
        this.setState({ initTooltipVisible: false });
	}

    createTransMode() {
        message.info("Click on two states to create a transition");
		window.fsm_editor.setCTMode();
	}

	parameterizeMode() {
		window.fsm_editor.setPMMode();
    }

    newModel() {
        window.fsm_editor.init();
    }

    importModel() {
        // load into editor window
        this.loadFromFile(this.state.fileList[0], (newmodel) => {
            // this is an error returned lmao
            if (typeof newmodel === 'string') {
                notification.error({
                    message: "Error loading model",
                    description: newmodel,
                    duration: 20
                });
                this.closeImportModal();
                return;
            }
            try {
                window.fsm_editor.import(newmodel);

                let sensors = [];
                // file was saved with extraprops, get sensor list
                if (newmodel.extraProps) {
                    sensors = newmodel.extraProps.sensors;
                } else {
                    // list sensors from model
                    let wsens = [];
                    newmodel.states.forEach(state => state.sensors.forEach(sensor => wsens.push(sensor.name)));
                    newmodel.transitions.forEach(transition => transition.sensors.forEach(sensor => wsens.push(sensor.name)));
                    // delete duplicated names
                    wsens = [...new Set(wsens)];
                    // generate sensors with ids
                    let wsensdic = {};
                    wsens.forEach(sensname => {
                        let sensid = window.nodeIDGenerator();
                        sensors.push({
                            id: sensid,
                            name: sensname
                        });
                        wsensdic[sensname] = sensid;
                    });
                    // propagate IDs back
                    newmodel.states.forEach(state => state.sensors.forEach(sensor => sensor.id = wsensdic[sensor.name]));
                    newmodel.transitions.forEach(transition => transition.sensors.forEach(sensor => sensor.id = wsensdic[sensor.name]));
                }

                this.setState({ sensors: sensors });
                this.closeImportModal();
            } catch (e) {
                notification.error({
                    message: "Error loading model, check that it is valid",
                    description: e.message,
                    duration: 20
                });
                this.closeImportModal();
            }
        });
    }

    export() {
        fetch('/bda-server/scxml/export', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                sensors: this.state.sensors,
                model: window.fsm_editor.model
            })
        }).then(response => response.text())
        .then(response => window.fsm_editor.exportSCXML(response));
    }

    exportJSON() {
        // use fsm_editor export function, passing it sensors for extraProps
        window.fsm_editor.exportJSON([{name: "sensors", data: this.state.sensors}]);
    }

	exportGenesisDialog() {
		this.validateParametrization();
		this.setState({ genesisDialogVisible: true });
		//window.fsm_editor.export();
	}

	exportGenesis() {
		console.log("Exporting to GeneSIS ID " + this.state.genesis.id + " with docker host IP " + this.state.genesis.ip);

        fetch('/bda-server/genesis/export', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                genesisip: this.state.genesis.genesisip,
                id: this.state.genesis.id,
                dockerip: this.state.genesis.dockerip,
                model: window.fsm_editor.model,
                sensors: this.state.sensors
            })
        }).then(response => response.json())
            .then(data => fetch("/bda-server/genesis/deploy", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    genesisip: this.state.genesis.genesisip,
                    id: this.state.genesis.id,
                    dockerip: this.state.genesis.dockerip,
                    model: data.genesisModel
                })
            }));
			//.then(data => window.fsm_editor.download("genesis_deployment_model.json", JSON.stringify(data.genesisModel)))
    }

    exportGenesisDownload() {
        console.log("Exporting to GeneSIS ID " + this.state.genesis.id + " with docker host IP " + this.state.genesis.ip);

        fetch('/bda-server/nodered/export', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                //genesisip: this.state.genesis.genesisip,
                id: this.state.genesis.id,
                dockerip: this.state.genesis.dockerip,
                model: window.fsm_editor.model,
                sensors: this.state.sensors
            })
        }).then(response => response.json())
            .then(data => window.fsm_editor.download("genesis_deployment_model.json", JSON.stringify(data.genesisModel)))
    }

    exportNodeRed() {
        console.log("Exporting to NodeRED");

        fetch('/bda-server/nodered/export', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: this.state.genesis.id,
                dockerip: this.state.genesis.dockerip,
                model: window.fsm_editor.model,
                sensors: this.state.sensors
            })
        })
            .then(response => response.json())
            .then(data => { console.log(data); return data; })
            .then(data => window.fsm_editor.download("BDANodeRED.json", JSON.stringify(data.genesisModel.dm.components.filter(cmp => cmp.name ==="BDANodeRED-flow")[0].nr_flow)));
    }

    exportNodeRedClipboard() {
        console.log("Exporting to NodeRED CB");

        fetch('/bda-server/nodered/export', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: this.state.genesis.id,
                dockerip: this.state.genesis.dockerip,
                model: window.fsm_editor.model,
                sensors: this.state.sensors
            })
        })
            .then(response => response.json())
            .then(data => { console.log(data); return data; })
            .then(data => {
                // firefox sucks
                // this can be unfucked when FF supports clipboard-write permission
                if (!navigator.clipboard) {
                    notification.error({
                        message: "Export to clipboard failed",
                        description: "Your browser does not support the asynchronous clipboard API, try Firefox or Chrome"
                    });
                } else {
                    notification.info({
                        message: "Export to clipboard pending",
                        description: (
                            <Button onClick={e =>
                                navigator.clipboard.writeText(JSON.stringify(data.genesisModel.dm.components.filter(cmp => cmp.name === "BDANodeRED-flow")[0].nr_flow))
                                    .then(function () {
                                        notification.destroy();
                                        notification.success({
                                            message: "Export to clipboard successful"
                                            //description:
                                        });
                                    }, function (err) {
                                        notification.destroy();
                                        notification.error({
                                            message: "Export to clipboard failed",
                                            description: err
                                        });
                                    })
                            }>CLICK THIS BUTTON TO COPY</Button>),
                        duration: 0
                    });
                }
            });
    }

    openSensorMgmtModal() {
        this.setState({ sensorMgmtModalVisible: true, sensorMgmtTempList: JSON.parse(JSON.stringify(this.state.sensors)) })
    }

    validateSensorConfiguration() {
        let newSensorList = this.state.sensorMgmtTempList;
        // sensor doesn't exist, add it
        if (newSensorList.filter(s => s.id === this.state.sensorMgmtTempSensor.id).length === 0) {
            // clear pending flag
            delete(this.state.sensorMgmtTempSensor.pending)
            newSensorList.push(this.state.sensorMgmtTempSensor);
        }
        this.setState({ sensorMgmtTempList: newSensorList });
    }

    validateSensorManagement() {
        this.setState({ sensors: this.state.sensorMgmtTempList, sensorManagementSelected: null });
    }

    nextSensorManagement() {
        // get sensor from list
        let sensor = this.state.sensorMgmtTempList.filter(s => s.id === this.state.sensorManagementSelected)[0];

        // sensor not in list, create new selected, initialize
        if (!sensor) {
            sensor = {
                id: window.nodeIDGenerator(),
                pending: true,
                name: ""
            };
        }
        this.setState({ sensorCfgModalVisible: true, sensorMgmtTempSensor: sensor });
    }

    deleteSensor() {
        this.setState({ sensorMgmtTempList: this.state.sensorMgmtTempList.filter(s => s !== this.state.sensorMgmtTempSensor), sensorCfgModalVisible: false, sensorManagementSelected: null });
    }

    cancelSensorConfiguration() {
        // sensor is a new one, juste delete it
        if (this.state.sensorMgmtTempSensor.pending) {
            this.setState({ sensorMgmtTempList: this.state.sensorMgmtTempList.filter(s => s !== this.state.sensorMgmtTempSensor) });
        } else {
            // discard changes by replacing sensor in list
            let newSensorList = this.state.sensorMgmtTempList;
            newSensorList[newSensorList.findIndex(s => s.id === this.state.sensorMgmtTempSensor.id)] = this.state.sensors.find(s => s.id === this.state.sensorMgmtTempSensor.id);
            this.setState({ sensorMgmtTempList: newSensorList });
        }
    }

    cancelSensorManagement() {
        this.setState({ sensorMgmtTempList: null });
    }

    updateSensorMgmtTempSensor(prop, value) {
        let sens = this.state.sensorMgmtTempSensor;
        sens[prop] = value;
        this.setState({ sensorMgmtTempSensor: sens });
    }

    handleSensorOnChange(e, parameter, sensorparam) {
        var toSet = null;
        // special cases to handle non Input
        if (e.target) {
            if (e.target.value) {
                // Input
                toSet = e.target.value;
            } else {
                // checkboxes
                if (e.target.checked !== null) {
                    toSet = e.target.checked;
                }
            }
        } else {
            // select have no target field but instead the key
            toSet = e;
        }

        let sens = this.state.sensorMgmtTempSensor;
        if (parameter === "sensors") {
            // base sensor data (name, constraint)
            sens[sensorparam] = toSet;
        } else {
            // sensor parameters
            sens.parameters[sensorparam] = toSet;
        }

        this.setState({ sensorMgmtTempSensor: sens });
    }

    openParametrizeWindow(target, handleParametrizeCB) {
		this.setState(
			{
				screen: "parametrize",
				parametrizeTarget: target,
				handleParametrizeCB: handleParametrizeCB,
				pendingParametrizationData: JSON.parse(JSON.stringify(target.data())), 
				currentParametrizationData: target.data()
			});
	}

    validateParametrization() {
        if (this.state.handleParametrizeCB) {
            this.state.handleParametrizeCB(this.state.pendingParametrizationData);
        }
		this.clearPendingParametrization();
	}

	clearPendingParametrization() {
		this.setState({ pendingParametrizationData: {}, screen: "welcome" });
    }

    deleteItem(sensoridx) {
        // sensoridx is undef for state/trans deletions
        if (sensoridx) {
            var ppd = this.state.pendingParametrizationData;
            ppd.sensors.splice(sensoridx, 1);
            this.setState({ pendingParametrizationData: ppd });
        } else {
            let param = this.state.pendingParametrizationData
            param.mode = "delete";
            this.state.handleParametrizeCB(param);
            this.clearPendingParametrization();
        }
    }

	validateCurveParametrization() {
        let newSens = this.state.sensorMgmtTempSensor;
        let ppd = this.state.pendingParametrizationData;
        ppd.sensors[ppd.sensors.findIndex(s => s.id === newSens.id)] = newSens;
        this.setState({ sensorMgmtTempSensor: null, pendingParametrizationData: ppd });
	}

	clearPendingCurveParametrization(sensoridx) {
		// clear tmp s
        this.setState({ sensorMgmtTempSensor: null });
	}

	handleParameterOnChange(e, parameter, sensoridx, sensorparam) {
		var ppd = this.state.pendingParametrizationData;

		var toSet = null;
		// special cases to handle non Input
		if (e.target) {
			if (e.target.value) {
				// Input
				toSet = e.target.value;
			} else {
				// checkboxes
				if (e.target.checked !== null) {
					toSet = e.target.checked;
				}
			}
		} else {
			// select have no target field but instead the key
			toSet = e;
		}

		if (parameter.startsWith("sensors")) {
            if (parameter === "sensors") {
                if (sensorparam) {
                    // base sensor data (name, constraint)
                    ppd.sensors[sensoridx][sensorparam] = toSet;
                } else {
                    // changed sensor id
                    // add empty params objects to avoid crashin
                    ppd.sensors[sensoridx] = {
                        ...this.state.sensors.filter(s => s.id === toSet)[0],
                        ...{
                            parameters: {}
                        }
                    };
                }
			} else {
				// sensor parameters
				ppd.sensors[sensoridx].parameters[sensorparam] = toSet;
			}
		} else {
			ppd[parameter] = toSet;
		}
		this.setState({ pendingParametrizationData: ppd });
	}

    openConfigurationModal(sensoridx) {
        this.setState({ configurationModalVisible: true, sensorMgmtTempSensor: JSON.parse(JSON.stringify(this.state.pendingParametrizationData.sensors[sensoridx])) });
	}

	addSensor() {
        let sensor = this.state.sensors[0];
        if (sensor) {
            var ppd = this.state.pendingParametrizationData;
            ppd.sensors.push({ id: sensor.id, constraint: "poss", parameters: {} });
            this.setState({ pendingParametrizationData: ppd });
        } else {
            notification.error({
                message: 'Cannot add sensor',
                duration: 0,
                description: 'You need to add a sensor first using the sensor management menu',
            });
        }
    }

	showImportModal() {
        this.setState({ importDialogVisible: true, initTooltipVisible: false });
    }

    closeImportModal() {
        this.setState({ importDialogVisible: false, fileList:[], disableUpload: false });
    }

    handleFileRead(callback) {
        return function () {
            var content = this.fileReader.result;
            let filename = this.state.fileList[0].name.split('\\').pop().split('/').pop();
            switch (filename.substr(filename.lastIndexOf(".") + 1)) {
                case "json":
                    callback(JSON.parse(content));
                    break;
                case "scxml":
                case "xml":
                    fetch('/bda-server/scxml/import', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            data: content
                        })
                    }).then(response => response.json())
                        .then(data => callback(data))
                    break;
                case "csv":
                case "dot":
                    callback(content);
                    break;
                default:
                    callback("invalid extension " + filename.substr(filename.lastIndexOf(".") + 1));
                    break;
            }
        }.bind(this);
	}

	loadFromFile(file, callback) {
        this.fileReader = new FileReader();
		this.fileReader.onloadend = this.handleFileRead(callback);
		this.fileReader.readAsText(file);
    }

    // json view
    handleChangeTab(key) {
        switch (key) {
            case "json":
                if (!this.state.editor) {
                    let container = document.getElementById("jsoneditor");
                    let options = {};
                    let editor = new window.JSONEditor(container, options);
                    editor.set(window.modelData ? window.modelData.model : {});
                    this.setState({ editor: editor });
                } else {
                    this.state.editor.set(window.fsm_editor.model);
                }
                break;
            case "nodes":
            default:
                break;
        }
    }

    startLoadSpinner() {
        this.clearLoadSpinner();
        this.setState({ loadSpinner: message.loading('Analysis in progress...', 0) });
    }

    clearLoadSpinner() {
        if (this.state.loadSpinner) {
            this.state.loadSpinner();
            this.setState({ loadSpinner: undefined });
        }
    }

    showParametersModal() {
        this.setState({ setParamModalVisible: true });
    }

    closeParametersModal() {
        this.setState({ setParamModalVisible: false });
    }

    saveParameters() {
        this.closeParametersModal();
    }

    showLoadObsModal() {
        this.setState({ loadObsModalVisible: true });
    }

    closeObsModal() {
        this.setState({ loadObsModalVisible: false, fileList: [], disableUpload: false });
    }

    loadObs() {
        this.loadFromFile(this.state.fileList[0], (obs) => {
            if (typeof obs !== 'string' || obs.startsWith("invalid")) {
                notification.error({
                    message: "Error loading obs file",
                    description: typeof obs !== 'string' ? "Tried to load a JSON/SCXML model" : "Tried to load a random file",
                    duration: 20
                });
                this.setState({ isObsLoaded: false });
                this.closeObsModal();
                return;
            }
            try {
                window.renderObs(obs);
                fetch('/bda-server/analysis/observations', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'text/plain'
                    },
                    body: obs
                }).then(response => response.json()).then(data => {
                    console.log(data);
                    if (data.ok) {
                        notification.success({
                            message: "Model saved successfully",
                            duration: 3
                        });
                    } else {
                        notification.error({
                            message: "Error saving model",
                            description: "" + data.error,
                            duration: 20
                        });
                    }
                });
                this.setState({ isObsLoaded: true });
                this.closeObsModal();
            } catch (e) {
                notification.error({
                    message: "Error loading obs file, check that it is valid",
                    description: e.message,
                    duration: 20
                });
                this.setState({ isObsLoaded: false });
                this.closeObsModal();
            }
        });
    }

    showLoadExpectedModal() {
        this.setState({ loadExpModalVisible: true });
    }

    closeExpectedModal() {
        this.setState({ loadExpModalVisible: false, fileList: [], disableUpload: false });
    }

    loadExpected() {
        this.loadFromFile(this.state.fileList[0], (expected) => {
            if (typeof expected !== 'string' || expected.startsWith("invalid")) {
                notification.error({
                    message: "Error loading expected file",
                    description: typeof expected !== 'string' ? "Tried to load a JSON/SCXML model" : "Tried to load a random file",
                    duration: 20
                });
                this.setState({ isExpLoaded: false });
                this.closeExpectedModal();
                return;
            }
            try {
                window.renderExpected(expected);
                fetch('/bda-server/analysis/expected', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'text/plain'
                    },
                    body: expected
                }).then(response => response.json()).then(data => {
                    console.log(data);
                    if (data.ok) {
                        notification.success({
                            message: "Model saved successfully",
                            duration: 3
                        });
                    } else {
                        notification.error({
                            message: "Error saving model",
                            description: "" + data.error,
                            duration: 20
                        });
                    }
                });
                this.setState({ isExpLoaded: true });
                this.closeExpectedModal();
            } catch (e) {
                notification.error({
                    message: "Error loading obs file, check that it is valid",
                    description: e.message,
                    duration: 20
                });
                this.setState({ isExpLoaded: false });
                this.closeExpectedModal();
            }
        });
    }

    showLoadDotModal() {
        this.setState({ loadDotModalVisible: true });
    }

    closeDotModal() {
        this.setState({ loadDotModalVisible: false, fileList: [], disableUpload: false });
    }

    loadDot() {
        this.loadFromFile(this.state.fileList[0], (dot) => {
            if (typeof dot !== 'string' || dot.startsWith("invalid")) {
                notification.error({
                    message: "Error loading dot file",
                    description: typeof dot !== 'string'?"Tried to load a JSON/SCXML model":"Tried to load a random file",
                    duration: 20
                });
                this.setState({ isDotLoaded: false });
                this.closeDotModal();
                return;
            }
            try {
                window.renderDot(dot);
                fetch('/bda-server/analysis/dot', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'text/plain'
                    },
                    body: dot
                }).then(response => response.json()).then(data => {
                    console.log(data);
                    if (data.ok) {
                        notification.success({
                            message: "Model saved successfully",
                            duration: 3
                        });
                    } else {
                        notification.error({
                            message: "Error saving model",
                            description: ""+data.error,
                            duration: 20
                        });
                    }
                });
                this.setState({ isDotLoaded: true });
                this.closeDotModal();
            } catch (e) {
                notification.error({
                    message: "Error loading model, check that it is valid",
                    description: e.message,
                    duration: 20
                });
                this.setState({ isDotLoaded: false });
                this.closeDotModal();
            }
        });
    }

    onStepSliderChange(value) {
        this.setState({
            sliderValue: value,
        });
        if (value === 0) {
            window.renderDot(window.saveddot);
        } else if (window.dotfiles[value]) {
            window.renderDot(window.dotfiles[value]);
        }
    }

    startAnalysis() {
        this.startLoadSpinner();
        fetch('/bda-server/analysis/run', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'text/plain'
            },
            body: JSON.stringify({ parameters: this.state.parameters })
        }).then(response => response.json()).then(data => {
            this.clearLoadSpinner();
            console.log(data);
            if (data.ok) {
                window.dotfiles = data.ok;
                window.renderDot(window.dotfiles[0]);
                window.renderAllDataI(data.all_data_i);
                window.renderAllDataO(data.all_data_o);
                notification.success({
                    message: "Analysis completed successfully",
                    description: "Steps: " + data.ok.length,
                    duration: 3
                });
                this.setState({ maxObservation: data.ok.length });
            } else {
                notification.error({
                    message: "Analysis failed",
                    description: "" + data.error,
                    duration: 20
                });
            }
        });
    }

    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };

        // drop down list of sensors
        let sensors = [<Option key="createnew" value="createnew">Create New...</Option>];
        if (this.state.sensorMgmtTempList) {
            this.state.sensorMgmtTempList.forEach(tmpSensor => {
                sensors.push(<Option key={tmpSensor.id} value={tmpSensor.id}>{tmpSensor.name}</Option>)
            });
        }

        // configuration fields, we need to do them here to avoid nulls 
        let sensorConfigurationInputs = [];
        if (this.state.sensorMgmtTempSensor) {
            sensorConfigurationInputs = (
                <div>
                    <Form.Item label="Name" {...formItemLayout}>
                        <Input value={this.state.sensorMgmtTempSensor.name} onChange={(e) => this.updateSensorMgmtTempSensor("name", e.target.value)} />
                    </Form.Item>
                </div>
            );
        }

        // file input configuration
        const props = {
            name: 'file',
            multiple: false,
            beforeUpload: (file) => {
                this.setState({ disableUpload: true, alertFileVisible: false });
                this.state.fileList.push(file);
                // return false to not actually upload from the Dragger, instead we upload manually on select button
                return false;
            },
            onRemove: (f) => {
                this.setState({ fileList: this.state.fileList.filter(fi => fi === f), disableUpload: false });
                return true;
            }
        };

        // actual layout
		return (
			<Layout theme="light">
				<Header theme="light" style={{ padding: '0px'}}>
                    <Menu theme="light" mode="horizontal" style={{ lineHeight: '64px', padding: '10px 0 10 0' }} selectable={false}>
                        <Menu.Item key="logo" disabled={true}><img style={{ height: "64px" }} src={this.state.headerPicture} alt="logo enact" /></Menu.Item>

                        {this.state.mode === "configuration" ?
                            /** configuration **/
                            [<SubMenu key="yes" title={<div><Icon type="file" /> <Tooltip placement="bottom" title="Or load a model" visible={this.state.initTooltipVisible} >FSM Model</Tooltip></div>}>
                                <Menu.Item key="new" onClick={this.newModel}>New</Menu.Item>
                                <Menu.Item key="load" onClick={this.showImportModal}>Import model</Menu.Item>
                                <Menu.Item key="exportmodel" onClick={this.export}>Export as SCXML</Menu.Item>
                                <Menu.Item key="exportjson" onClick={this.exportJSON}>Export as editor model</Menu.Item>
                            </SubMenu>,
                            <SubMenu key="no" title={<div><Icon type="cluster" />Deploy</div>}>
                                <Menu.Item key="exportgenesis" onClick={this.exportGenesisDialog}>Deploy to GeneSIS</Menu.Item>
                                <Menu.Item key="exportgenesisfile" onClick={this.exportGenesisDownload}>Export as GeneSIS deployment model to file</Menu.Item>
                                <Menu.Item key="exportnodered" onClick={this.exportNodeRed}>Export as Node-RED flow to file</Menu.Item>
                                <Menu.Item key="exportnoderedcb" onClick={this.exportNodeRedClipboard}>Export as Node-RED flow to clipboard</Menu.Item>
                            </SubMenu>
                            ] :
                            /** analysis **/
                            [
                                <Menu.Item key="loaddot" onClick={this.showLoadDotModal}><Icon type="file" /> Load dot</Menu.Item>,
                                <Menu.Item key="loadexp" onClick={this.showLoadExpectedModal} disabled={!this.state.isDotLoaded}><Icon type="file" /> Load expected behaviour</Menu.Item>,
                                <Menu.Item key="loadobs" onClick={this.showLoadObsModal} disabled={!this.state.isDotLoaded}><Icon type="file" /> Load field behaviour</Menu.Item>,
                                <Menu.Item key="setparameters" onClick={this.showParametersModal} disabled={!this.state.isDotLoaded}><Icon type="file" /> Set Parameters</Menu.Item>
                            ]
                        }

                        <Menu.Item style={{ float: "right", height: "64px" }} key="logocnrs" disabled={true}>
                            <Switch checkedChildren="     Analysis     " unCheckedChildren="Configuration" onChange={this.changeMode} />
                            <img style={{ paddingLeft: "64px", float: "right", height: "64px" }} src="img/cnrs.png" alt="logo cnrs" />
                        </Menu.Item>
					</Menu>
				</Header>
                <Layout>
                    {this.state.mode === "configuration" ?
                        <Sider theme="dark" width="150" /*style={{ padding: "10px", marginTop: "1px" }}*/ collapsible collapsed={this.state.menuCollapsed} onCollapse={e => this.setState({ menuCollapsed: e })}>
                            <Tooltip placement="right" title="Add a state to start" visible={this.state.initTooltipVisible} id="initTooltip">
                                <Menu theme="dark" mode="inline" selectable={false} inlineCollapsed={this.state.menuCollapsed}>
                                    <Menu.Item key="sensormgmt" onClick={this.openSensorMgmtModal}><Icon type="deployment-unit" /><span>Sensors</span></Menu.Item>
                                    <SubMenu key="add" title={<div><Icon type="edit" /><span>Add</span></div>}>
                                        <Menu.Item onClick={this.createStateMode}>State</Menu.Item>
                                        <Menu.Item onClick={this.createTransMode}>Transition</Menu.Item>
                                    </SubMenu>
                                </Menu>
                            </Tooltip>
                        </Sider>
                        : null}
					<Layout>
                        <Content>
                            {/** configuration **/}
                            {this.state.mode === "configuration" ?
                                [
                                    <Modal
                                        title="Sensor Management"
                                        key={0}
                                        visible={this.state.sensorMgmtModalVisible}
                                        onCancel={() => { this.cancelSensorManagement(); this.setState({ sensorMgmtModalVisible: false }) }}
                                        footer={
                                            <div>
                                                <Button onClick={() => { this.cancelSensorManagement(); this.setState({ sensorMgmtModalVisible: false }) }}>Cancel</Button>
                                                <Button onClick={() => { this.nextSensorManagement(); }}>Next <Icon type="right" /></Button>
                                                <Button onClick={() => { this.validateSensorManagement(); this.setState({ sensorMgmtModalVisible: false }) }} type="primary">Validate</Button>
                                            </div>
                                        }>

                                        <Select style={{ width: "100%" }} value={this.state.sensorManagementSelected} onChange={(e) => this.setState({ sensorManagementSelected: e })} >
                                            {sensors}
                                        </Select>
                                    </Modal>,
                                    <Modal
                                        title="Sensor Configuration"
                                        key={1}
                                        visible={this.state.sensorCfgModalVisible}
                                        onOk={() => { this.validateSensorConfiguration(); this.setState({ sensorCfgModalVisible: false }) }}
                                        onCancel={() => { this.cancelSensorConfiguration(); this.setState({ sensorCfgModalVisible: false }) }}>

                                        {sensorConfigurationInputs}
                                    </Modal>,
                                    <Modal
                                        title="Parameter entry"
                                        key={2}
                                        visible={this.state.configurationModalVisible}
                                        onOk={() => { this.setState({ configurationModalVisible: false }); this.validateCurveParametrization(); }}
                                        onCancel={() => { this.setState({ configurationModalVisible: false }); this.clearPendingCurveParametrization(); }}
                                    >

                                        <FSMConfigurationFields
                                            screen="parameters"
                                            sensorToConfigure={this.state.sensorMgmtTempSensor}
                                            onChange={this.handleSensorOnChange} />
                                    </Modal>,
                                    <Modal
                                        title="GeneSIS Configuration"
                                        key={3}
                                        visible={this.state.genesisDialogVisible}
                                        onOk={() => { this.exportGenesis(); this.setState({ genesisDialogVisible: false }) }}
                                        onCancel={() => { this.setState({ genesisDialogVisible: false }) }}>

                                        <Input addonBefore="Genesis host IP" value={this.state.genesis.genesisip} onChange={e => { let g = this.state.genesis; g.genesisip = e.target.value; this.setState({ genesis: g }); }} />
                                        <Input addonBefore="BDA Node-RED flow ID" value={this.state.genesis.id} onChange={e => { let g = this.state.genesis; g.id = e.target.value; this.setState({ genesis: g }); }} />
                                        <Input addonBefore="Docker host IP" value={this.state.genesis.dockerip} onChange={e => { let g = this.state.genesis; g.dockerip = e.target.value; this.setState({ genesis: g }); }} />
                                    </Modal>,
                                    <Modal
                                        title="Import Model"
                                        key={4}
                                        visible={this.state.importDialogVisible}
                                        onOk={this.importModel}
                                        onCancel={this.closeImportModal}>

                                        <p>Load model from scxml or json:</p>
                                        {/**<Input type="file" addonBefore="Model file" id="modelfile" accept=".json,.xml" value={this.state.tempLoadModelPath} onChange={e => { this.setState({ tempLoadModelPath: e.target.value }); this.handleFileChosen(e.target.files[0]) }} />**/}
                                        <div id="sel-file-dragger">
                                            <Dragger {...props} disabled={this.state.disableUpload} fileList={this.state.fileList}>
                                                <p className="ant-upload-drag-icon">
                                                    <Icon type="inbox" />
                                                </p>
                                                <p className="ant-upload-text">Click to open dialog, or drag environment model to this area to load a model</p>
                                            </Dragger>
                                        </div>
                                    </Modal>,
                                    <Tabs onChange={this.handleChangeTab} tabPosition='right' style={{ paddingTop: "10px" }} key={5}>
                                        <TabPane tab={<span><Icon type="share-alt" />Graph View</span>} key="nodes">
                                            <div id="fsm_editor" />
                                            <div id="cy_nav" />
                                        </TabPane>
                                        <TabPane forceRender={true} tab={<span><Icon type="codepen" />JSON View</span>} key="json">
                                            <div id="fsm_editor_root">
                                                <div id="jsoneditorbuttoncontainer">
                                                    <Button type="primary" id="saveJSON"><Icon type="save" />Save</Button>
                                                </div>
                                                <div id="jsoneditor"></div>
                                            </div>
                                        </TabPane>
                                    </Tabs>
                                ] :
                                /** analysis **/
                                [
                                    <div id="stepslidercontainer" key="sstepslidercontainer">
                                        <Row key="slider" id="stepslider">
                                            <Col span={2}></Col>
                                            <Col span={12}>

                                            </Col>
                                            <Col span={4}>

                                            </Col>
                                            <Col span={4}>
                                                <Button type="primary" onClick={this.startAnalysis} disabled={!(this.state.isDotLoaded && this.state.isObsLoaded && this.state.isExpLoaded)}>Analyze</Button>
                                                <Button onClick={window.changeDotEngine}>Change engine</Button>
                                            </Col>
                                        </Row>
                                    </div>,
                                    <div id="analysisroot" key="analysisroot">
                                        <div id="analysisdot"></div>
                                        <div id="all_data_i_canvas" className="graphcanvas"></div>
                                        <div id="obscanvasparent">
                                            <div id="obscanvas" className="graphcanvas2"></div>
                                            <div id="obscanvas2" className="graphcanvas2"></div>
                                        </div> 
                                        <div id="all_data_o_canvas" className="graphcanvas"></div>
                                    </div>,
                                    <Modal
                                        title="Load dot"
                                        key="importdot"
                                        visible={this.state.loadDotModalVisible}
                                        onOk={this.loadDot}
                                        onCancel={this.closeDotModal}>

                                        <p>Load dot file</p>
                                        <div id="sel-file-dragger">
                                            <Dragger {...props} disabled={this.state.disableUpload} fileList={this.state.fileList}>
                                                <p className="ant-upload-drag-icon">
                                                    <Icon type="inbox" />
                                                </p>
                                                <p className="ant-upload-text">Click to open dialog, or drag dot file.</p>
                                            </Dragger>
                                        </div>
                                    </Modal>,
                                    <Modal
                                        title="Load field behaviour"
                                        key="importobs"
                                        visible={this.state.loadObsModalVisible}
                                        onOk={this.loadObs}
                                        onCancel={this.closeObsModal}>

                                        <p>Load field behaviour file</p>
                                        <div id="sel-file-dragger">
                                            <Dragger {...props} disabled={this.state.disableUpload} fileList={this.state.fileList}>
                                                <p className="ant-upload-drag-icon">
                                                    <Icon type="inbox" />
                                                </p>
                                                <p className="ant-upload-text">Click to open dialog, or drag observations (csv) file.</p>
                                            </Dragger>
                                        </div>
                                    </Modal>,
                                    <Modal
                                        title="Load expected behaviour"
                                        key="importexp"
                                        visible={this.state.loadExpModalVisible}
                                        onOk={this.loadExpected}
                                        onCancel={this.closeExpectedModal}>

                                        <p>Load expected behaviour file</p>
                                        <div id="sel-file-dragger">
                                            <Dragger {...props} disabled={this.state.disableUpload} fileList={this.state.fileList}>
                                                <p className="ant-upload-drag-icon">
                                                    <Icon type="inbox" />
                                                </p>
                                                <p className="ant-upload-text">Click to open dialog, or drag expected behaviour (csv) file.</p>
                                            </Dragger>
                                        </div>
                                    </Modal>,
                                    <Modal
                                        title="Set Parameters"
                                        key="setparamsmodal"
                                        visible={this.state.setParamModalVisible}
                                        onOk={this.saveParameters}
                                        closable={false}
                                        footer={[<Button key="saveparams" type="primary" onClick={this.saveParameters}>Save</Button>] }>
                                        
                                        <p>Parameters</p>
                                        <Form>
                                            <Form.Item label="Delimiter">
                                                <Input key="sep" value={this.state.parameters.delimiter} onChange={(ev) => { let p = this.state.parameters; p.delimiter = ev.target.value; this.setState({ parameters: p }); }} />
                                            </Form.Item>
                                            <Form.Item label="MIN_SAMPLES_I">
                                                <Input key="in" value={this.state.parameters.MIN_SAMPLES_I} onChange={(ev) => { let p = this.state.parameters; p.MIN_SAMPLES_I = ev.target.value; this.setState({ parameters: p }); }} />
                                            </Form.Item>
                                            <Form.Item label="MIN_SAMPLES_O">
                                                <Input key="out" value={this.state.parameters.MIN_SAMPLES_O} onChange={(ev) => { let p = this.state.parameters; p.MIN_SAMPLES_O = ev.target.value; this.setState({ parameters: p }); }} />
                                            </Form.Item>
                                        </Form>
                                    </Modal>
                                ]
                            }
						</Content>
                    </Layout>
                    {this.state.mode === "configuration" ?
                        <Sider theme="light" width="300" style={{ padding: "10px", marginTop: "1px" }}>
                            <FSMConfigurationFields
                                screen={this.state.screen}
                                itemToConfigure={this.state.pendingParametrizationData}
                                onChange={this.handleParameterOnChange}
                                openConfigurationModal={this.openConfigurationModal}
                                addSensorCallback={this.addSensor}
                                validateParametrization={this.validateParametrization}
                                clearPendingParametrization={this.clearPendingParametrization}
                                deleteItem={this.deleteItem}
                                sensors={this.state.sensors}
                            />
                        </Sider>
                        : null}
                </Layout>
                <Footer style={{ textAlign: 'center', height: "4vh", padding: "1vh" }}>ENACT &copy;2018-2020 Created by Univ. Côte d'Azur - CNRS</Footer>
			</Layout>

		);
	}


	componentDidMount() {
        this.initEditor();
    }

    renderDot() {
        window.renderDot();
    }
}
export default BDAConfigurator;