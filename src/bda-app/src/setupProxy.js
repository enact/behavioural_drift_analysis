﻿// https://facebook.github.io/create-react-app/docs/proxying-api-requests-in-development#configuring-the-proxy-manually

const proxy = require('http-proxy-middleware');

module.exports = function (app) {
	app.use(proxy('/bda-server', { target: 'http://localhost:3003/' }));
};