﻿import React from 'react';
import SensorConfigurationFields from './SensorConfigurationFields';
import { Input, Card, Form, Checkbox, Button, Icon, Select } from 'antd';
const { Option } = Select;

const FSMConfigurationFields = ({ screen, itemToConfigure, sensorToConfigure, onChange, openConfigurationModal, validateParametrization, clearPendingParametrization, addSensorCallback, deleteItem, sensors }) => {
	switch (screen) {
        case "parametrize": return itemConfigurationField(itemToConfigure, onChange, openConfigurationModal, validateParametrization, clearPendingParametrization, addSensorCallback, deleteItem, sensors);
		case "parameters": return curveConfig(sensorToConfigure, onChange);
		default: return (<p></p>)
	}
};


function itemConfigurationField(itemToConfigure, onChange, openConfigurationModal, validateParametrization, clearPendingParametrization, addSensorCallback, deleteItem, sensors) {
    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };
    const formWideItemLayout = {
        labelCol: {
            xs: { span: 0 },
            sm: { span: 0 },
        },
        wrapperCol: {
            xs: { span: 48 },
            sm: { span: 24 },
        },
    }

    // generate list of sensors
    let sensorOptions = [];
    sensors.forEach(sensor => sensorOptions.push(<Option key={sensor.id} value={sensor.id}>{sensor.name}</Option>));

    // generate sensor cards
    let sensorCards = [];
    // eslint is retarded
    // eslint-disable-next-line
    for (let sensoridx in itemToConfigure.sensors) {
        sensorCards.push(
            <Card key={"card" + sensoridx}>
                <Form.Item label="Name" {...formItemLayout}>
                    <Select value={itemToConfigure.sensors[sensoridx].id} onChange={(e) => onChange(e, "sensors", sensoridx)} >
                        {sensorOptions}
                    </Select>
                </Form.Item>
                <Form.Item label="Constraint" {...formItemLayout}>
                    <Select value={itemToConfigure.sensors[sensoridx].constraint} onChange={(e) => onChange(e, "sensors", sensoridx, "constraint")} >
                        <Option value="prob">Probability</Option>
                        <Option value="poss">Possibility</Option>
                    </Select>
                </Form.Item>
                <Button onClick={() => { openConfigurationModal(sensoridx); }}><Icon type="setting" /></Button>
                <Button onClick={() => { deleteItem(sensoridx); }}><Icon type="delete" theme="twoTone" twoToneColor="#ff0000"/></Button>
            </Card>
        );
    }

    switch (itemToConfigure.type) {
        case "transition":
            return (
                <Form {...formItemLayout} layout="vertical">
                    <Form.Item label="Name">
                        <Input value={itemToConfigure.label} onChange={(e) => onChange(e, "label")} />
                    </Form.Item>
                    <Form.Item label="From">
                        <Input value={itemToConfigure.source} onChange={(e) => onChange(e, "source")} />
                    </Form.Item>
                    <Form.Item label="To">
                        <Input value={itemToConfigure.target} onChange={(e) => onChange(e, "target")} />
                    </Form.Item>
                    <Form.Item label="Sensors">
                        <Button onClick={addSensorCallback}><Icon type="plus" /></Button>
                    </Form.Item>
                    <Form.Item {...formWideItemLayout}>
                        {sensorCards}
                    </Form.Item>
                    <Button type="primary" onClick={() => { validateParametrization(); }}>OK</Button>
                    <Button type="default" onClick={() => { clearPendingParametrization(); }}>Cancel</Button>
                    <Button type="danger" onClick={() => { deleteItem(); }}>Delete</Button>
                </Form>
            )

        case "state":
            return (
                <Form {...formItemLayout} layout="vertical">
                    <Form.Item label="Name">
                        <Input value={itemToConfigure.label} onChange={(e) => onChange(e, "label")} />
                    </Form.Item>
                    <Form.Item label="Loopback">
                        <Checkbox checked={itemToConfigure.loopback} onChange={(e) => onChange(e, "loopback")} />
                    </Form.Item>
                    <Form.Item label="Sensors">
                        <Button onClick={addSensorCallback}><Icon type="plus" /></Button>
                    </Form.Item>
                    <Form.Item {...formWideItemLayout}>
                        {sensorCards}
                    </Form.Item>
                    <Button type="primary" onClick={() => { validateParametrization(); }} style={{ width: "33%" }}><Icon type="save"/>Save</Button>
                    <Button type="default" onClick={() => { clearPendingParametrization(); }} style={{ width: "32%" }}>Cancel</Button>
                    <Button type="danger" onClick={() => { deleteItem(); }} style={{ width: "33%" }}><Icon type="delete"/>Delete</Button>
                </Form>
            )
        default:
            return (
                <div>no</div>
            )
    }
}

function curveConfig(sensorToConfigure, onChange) {
    var sensor = sensorToConfigure;
    if (sensor && sensor.parameters) {
        switch (sensor.constraint) {
            case "prob":
                return (
                    <Form>
                        <Form.Item label="Type">
                            <Select value={sensor.parameters.constraint} onChange={(e) => onChange(e, "sensors.parameters", "constraint")} >
                                <Option value="normal">Normal</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Mean">
                            <Input value={sensor.parameters.mean} onChange={(e) => onChange(e, "sensors.parameters", "mean")} />
                        </Form.Item>
                        <Form.Item label="SD">
                            <Input value={sensor.parameters.sd} onChange={(e) => onChange(e, "sensors.parameters", "sd")} />
                        </Form.Item>
                    </Form>);
            case "poss":
                return (
                    <Form>
                        <Form.Item label="Type">
                            <Select value={sensor.parameters.constraint} onChange={(e) => onChange(e, "sensors.parameters", "constraint")} >
                                <Option value="rampup">Ramp-Up</Option>
                                <Option value="rampdown">Ramp-Down</Option>
                                <Option value="trapezoid">Trapezoid</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Invert">
                            <Checkbox checked={sensor.parameters.invert} onChange={(e) => onChange(e, "sensors.parameters", "invert")} />
                        </Form.Item>
                        <SensorConfigurationFields onChange={onChange} sensor={sensor} />
                    </Form>);
            default: return (<div>Invalid constraint</div>);
        }
    } else {
        return (<div> no sensor selected </div>);
    }
}

export default FSMConfigurationFields;