﻿import React from 'react';
import { Input, Form } from 'antd';

const SensorConfigurationFields = ({ sensor, onChange }) => {
	switch (sensor.parameters.constraint) {
		case "rampdown": return (
			<div>
				<Form.Item label="Mode">
					<Input value={sensor.parameters.mode} onChange={(e) => onChange(e, "sensors.parameters", "mode")} />
				</Form.Item>
				<Form.Item label="Max">
					<Input value={sensor.parameters.max} onChange={(e) => onChange(e, "sensors.parameters", "max")} />
				</Form.Item>
			</div>
		);
		case "rampup": return (
			<div>
				<Form.Item label="Min">
					<Input value={sensor.parameters.min} onChange={(e) => onChange(e, "sensors.parameters", "min")} />
				</Form.Item>
				<Form.Item label="Mode">
					<Input value={sensor.parameters.mode} onChange={(e) => onChange(e, "sensors.parameters", "mode")} />
				</Form.Item>
			</div>
		);
		case "trapezoid": return (
			<div>
				<Form.Item label="Min">
					<Input value={sensor.parameters.min} onChange={(e) => onChange(e, "sensors.parameters", "min")} />
				</Form.Item>
				<Form.Item label="Mode 1">
					<Input value={sensor.parameters.mode1} onChange={(e) => onChange(e, "sensors.parameters", "mode1")} />
				</Form.Item>
				<Form.Item label="Mode 2">
					<Input value={sensor.parameters.mode2} onChange={(e) => onChange(e, "sensors.parameters", "mode2")} />
				</Form.Item>
				<Form.Item label="Max">
					<Input value={sensor.parameters.max} onChange={(e) => onChange(e, "sensors.parameters", "max")} />
				</Form.Item>
			</div>
		);
		default: return (<div>Select a constraint</div>);
	}
};

export default SensorConfigurationFields;