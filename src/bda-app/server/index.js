const express = require('express')
const fs = require("fs");
const util = require('util');
const crypto = require('crypto');
const xml2js = require("xml2js");
const http = require("http");
const URL = require('url').URL;

const exec = require('child_process').exec;

const app = express();
const port = 3003;

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

// enable json body
app.use(express.json());

// enable swagger doc
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// export 
app.post('/bda-server/genesis/export', exportModelAsGenesis);
app.post('/bda-server/nodered/export', exportNodeRED);
// deploy 
app.post('/bda-server/genesis/deploy', deployGenesis);
// convert from scxml to model
app.post('/bda-server/scxml/import', importSCXML);
// convert from molde to scxml
app.post('/bda-server/scxml/export', exportSCXML);

// select dot file
app.post('/bda-server/analysis/dot', [express.raw({ type: "text/plain" })], saveDotFile);
app.post('/bda-server/analysis/observations', [express.raw({ type: "text/plain" })], saveObsFile);
app.post('/bda-server/analysis/expected', [express.raw({ type: "text/plain" })], saveExpectedFile);
app.post('/bda-server/analysis/run', [express.raw({ type: "text/plain" })], runAnalysis);

function saveDotFile(req, res) {
    try {
        fs.writeFileSync("analysis_data/model.dot", req.body);
        res.end(JSON.stringify({ok:"ok"}));
    } catch (e) {
        res.end(JSON.stringify({error:e.message}));
    }
}

function saveObsFile(req, res) {
    try {
        fs.writeFileSync("analysis_data/observations.csv", req.body);
        res.end(JSON.stringify({ ok: "ok" }));
    } catch (e) {
        res.end(JSON.stringify({ error: e.message }));
    }
}

function saveExpectedFile(req, res) {
    try {
        fs.writeFileSync("analysis_data/expected.csv", req.body);
        res.end(JSON.stringify({ ok: "ok" }));
    } catch (e) {
        res.end(JSON.stringify({ error: e.message }));
    }
}

function runAnalysis(req, res) {
    try {
        console.log("BDA starting analysis");
        let parameters = JSON.parse(req.body).parameters || { delimiter: ";", MIN_SAMPLES_I: 40, MIN_SAMPLES_O: 40 };
        console.log("BDA parameters:", parameters);

        // start by deleting the existing files just in case
        fs.readdirSync("model_pictures").forEach(mp => fs.unlinkSync("model_pictures/" + mp));
        console.log("BDA model pictures deleted");

        // create observations
        console.log('python ../bda-analysis/bda.py -d analysis_data/model.dot -u analysis_data/observations.csv -e analysis_data/expected.csv -s "' + parameters.delimiter + "\" -i " + parameters.MIN_SAMPLES_I + " -o " + parameters.MIN_SAMPLES_O);
        let child = exec('python ../bda-analysis/bda.py -d analysis_data/model.dot -u analysis_data/observations.csv -e analysis_data/expected.csv -s "' + parameters.delimiter  + "\" -i " + parameters.MIN_SAMPLES_I + " -o " + parameters.MIN_SAMPLES_O);
        //let child = exec("sleep 10");
        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
        child.on('exit', function () {
            console.log("BDA analysis done");
            let ret = [];
            /*let models = fs.readdirSync("model_pictures");
            models.sort(naturalCompare);
            models.forEach(model => {
                if (model.endsWith(".dot")) {
                    ret.push(fs.readFileSync("model_pictures/" + model, "utf8"));
                } else {
                    //console.log("skipping " + model);
                }
            })*/
            ret.push(fs.readFileSync("../bda-analysis/dissimilarity_graph.dot", "utf8"));
            res.end(JSON.stringify({
                ok: ret,
                all_data_i: fs.readFileSync("../bda-analysis/all_data_i.csv", "utf8"),
                all_data_o: fs.readFileSync("../bda-analysis/all_data_o.csv", "utf8")
            }));
        });
    } catch (e) {
        res.end(JSON.stringify({ error: e.message }));
    }
}

//https://stackoverflow.com/questions/15478954/sort-array-elements-string-with-numbers-natural-sort/15479354#15479354
function naturalCompare(a, b) {
    var ax = [], bx = [];

    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

    while (ax.length && bx.length) {
        var an = ax.shift();
        var bn = bx.shift();
        var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
        if (nn) return nn;
    }

    return ax.length - bx.length;
}

function deployGenesis(sreq, res) {
    console.log("Deploying to GeneSIS :::");
    console.log(sreq.body.genesisip);

    let data = JSON.stringify(sreq.body.model);
    //console.log(data);
    let url = new URL(sreq.body.genesisip);
    let options = {
        path: "/genesis/deploy",
        host: url.hostname,
        port: url.port,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(data)
        }
    };
    let req = http.request(options, function (httpIncomingMessage) {
        let genesisDeployResponse = "";
        console.log("statusCode: ", httpIncomingMessage.statusCode);
        httpIncomingMessage.setEncoding('utf8');
        httpIncomingMessage.on('data', (chunk) => {
            genesisDeployResponse += chunk;
        });
        httpIncomingMessage.on('end', () => {
            //console.log("response " + genesisDeployResponse);
            res.end("ok");
        });
    });
    req.on('error', (e) => {
        console.log(e);
        res.end("nok");
    });
    req.write(data);
    req.end();
}

function exportSCXML(req, res) {
    let model = req.body.model;
    let fullsensors = req.body.sensors;
    res.type("text/xml");
    res.end(generateSCXML(model, fullsensors));
}

function generateSCXML(model, fullsensors) {
    let xmlModel = '<?xml version="1.0" encoding="UTF-8"?>\n<scxml xmlns="http://www.w3.org/2005/07/scxml" version="1.0" binding="early" xmlns:bda="https://www.enact-project.eu/bda" name="fsm-editor">\n';
    for (let stateidx in model.states) {
        let state = model.states[stateidx];
        let sensors = JSON.parse(JSON.stringify(state.sensors));
        // add name back
        sensors.forEach(sensor => {
            let fulls = fullsensors.find(s => s.id === sensor.id);
            sensor.name = fulls.name;
        });
        xmlModel += '<state id="' + state.id + '">\n\t<bda:sensors>' + JSON.stringify(sensors) + '</bda:sensors>\n\t<bda:label>' + state.label + '</bda:label>\n';

        for (let transidx in model.transitions) {
            let trans = model.transitions[transidx];
            let sensors = JSON.parse(JSON.stringify(trans.sensors));
            // add name back
            sensors.forEach(sensor => {
                let fulls = fullsensors.find(s => s.id === sensor.id);
                sensor.name = fulls.name;
            });
            if (trans.source === state.id) {
                xmlModel += '\t\t<transition id="' + trans.id + '" target="' + trans.target + '">\n\t\t<bda:sensors>' + JSON.stringify(sensors) + '</bda:sensors>\n\t\t</transition>\n'
            }
        }

        xmlModel += '</state>\n';
    }
    xmlModel += "</scxml>";

    return xmlModel;
}

function importSCXML(req, res) {
    let str = req.body.data;
    let parser = new xml2js.Parser();

    parser.parseString(str, function (err, template) {
        let modelRet = {
            states: [],
            transitions: []
        };
        template.scxml.state.forEach(state => {
            let wstate = {
                id: state.$.id,
                type: "state",
                label: state["bda:label"] ? state["bda:label"][0]:"",
                loopback: false,
                sensors: state["bda:sensors"]?JSON.parse(state["bda:sensors"]):[]
            };

            state.transition.forEach(t => {
                if (state.$.id === t.$.target) {
                    wstate.loopback = true;
                }
                modelRet.transitions.push({
                    id: t.$.id,
                    type: "transition",
                    source: state.$.id,
                    target: t.$.target,
                    sensors: t["bda:sensors"]?JSON.parse(t["bda:sensors"]):[]
                })
            });

            modelRet.states.push(wstate);
        });

        res.type("application/json");
        res.end(JSON.stringify(modelRet));
    });
}

function exportNodeRED(req, res) {
    let config = req.body;
    exportGenesisStage2(res, config, { dm: { containments: [], components: [] }});
}

function exportModelAsGenesis(req, res) {
    var config = req.body;
    console.log("Exporting to GeneSIS model:::");
    console.log(config.dockerip);
    console.log(config.genesisip);
    console.log(config.id);
    //console.log(config.model);

    // get current genesis model
    let url = new URL(config.genesisip);
    let options = {
        // here we use model_ui because that's what genesis exports, and starts with a dm/graph node
        // todo maybe work on non ui models?
        path: "/genesis/model_ui",
        host: url.hostname,
        port: url.port,
        method: 'GET'
    };
    let genesisreq = http.request(options, function (httpIncomingMessage) {
        let genesisResponse = "";
        httpIncomingMessage.setEncoding('utf8');
        httpIncomingMessage.on('data', (chunk) => {
            genesisResponse += chunk;
        });
        httpIncomingMessage.on('end', () => {
            // hand over to stage 2 for injection
            exportGenesisStage2(res, config, JSON.parse(genesisResponse));
        });
    });
    genesisreq.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });
    genesisreq.end();
}

function exportGenesisStage2(res, config, template) {
    // template components
    let dockercmp = {
        "_type": "/infra/docker_host",
        "name": "BDAHost",
        "properties": [],
        "version": "0.0.1",
        "id": uuidgen(),
        "provided_execution_port": [{
            "name": "offerDocker",
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            }
        }
        ],
        "ip": config.dockerip,
        "port": ["2375"],
        "credentials": {
            "username": "ubuntu",
            "password": "",
            "sshkey": "",
            "agent": ""
        },
        "monitoring_agent": "none"
    };

    let noderedondocker = {
        "name": "NodeRED_on_Docker",
        "properties": [],
        "src": "/BDAHost/offerDocker",
        "target": "/BDANodeRED/demandDocker"
    };

    let noderedcmp = {
        "_type": "/internal/node_red",
        "name": "BDANodeRED",
        "properties": [],
        "version": "0.0.1",
        "id": uuidgen(),
        "provided_execution_port": [{
            "name": "offerNodeRED",
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            }
        }
        ],
        "docker_resource": {
            "name": uuidgen(),
            "image": "enactproject/bda-engine:0.29",
            "command": "",
            "links": [],
            "port_bindings": {
                "1880": "1880"
            },
            "devices": {
                "PathOnHost": "",
                "PathInContainer": "",
                "CgroupPermissions": "rwm"
            }
        },
        "ssh_resource": {
            "name": uuidgen(),
            "startCommand": "",
            "downloadCommand": "",
            "installCommand": "",
            "configureCommand": "",
            "stopCommand": "",
            "credentials": {
                "username": "ubuntu",
                "password": "",
                "sshkey": "",
                "agent": ""
            }
        },
        "ansible_resource": {
            "name": uuidgen(),
            "playbook_path": "",
            "playbook_host": "",
            "credentials": {
                "username": "ubuntu",
                "password": "",
                "sshkey": "",
                "agent": ""
            }
        },
        "required_execution_port": {
            "name": "demandDocker",
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            },
            "needDeployer": false
        },
        "provided_communication_port": [{
            "name": uuidgen(),
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            },
            "port_number": "1880"
        }
        ],
        "required_communication_port": [{
            "name": uuidgen(),
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            },
            "port_number": "80",
            "isMandatory": false
        }
        ],
        "nr_flow": [],
        "path_flow": "",
        "packages": []
    };

    let flowonnodered = {
        "name": "NodeRED-flow_on_NodeRED",
        "properties": [],
        "src": "/BDANodeRED/offerNodeRED",
        "target": "/BDANodeRED-flow/demandNodeRED"
    };

    let nrflowcmp = {
        "_type": "/internal/node_red_flow",
        "name": "BDANodeRED-flow",
        "properties": [],
        "version": "0.0.1",
        "id": uuidgen(),
        "provided_execution_port": [{
            "name": uuidgen(),
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            }
        }
        ],
        "docker_resource": {
            "name": uuidgen(),
            "image": "",
            "command": "",
            "links": [],
            "port_bindings": {
                "1880": "1880"
            },
            "devices": {
                "PathOnHost": "",
                "PathInContainer": "",
                "CgroupPermissions": "rwm"
            }
        },
        "ssh_resource": {
            "name": uuidgen(),
            "startCommand": "",
            "downloadCommand": "",
            "installCommand": "",
            "configureCommand": "",
            "stopCommand": "",
            "credentials": {
                "username": "ubuntu",
                "password": "",
                "sshkey": "",
                "agent": ""
            }
        },
        "ansible_resource": {
            "name": uuidgen(),
            "playbook_path": "",
            "playbook_host": "",
            "credentials": {
                "username": "ubuntu",
                "password": "",
                "sshkey": "",
                "agent": ""
            }
        },
        "required_execution_port": {
            "name": "demandNodeRED",
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            },
            "needDeployer": false
        },
        "provided_communication_port": [{
            "name": uuidgen(),
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            },
            "port_number": "80"
        }
        ],
        "required_communication_port": [{
            "name": uuidgen(),
            "capabilities": {
                "_type": "/capability/security_capability",
                "name": "a_capability",
                "control_id": "",
                "description": ""
            },
            "port_number": "1880",
            "isMandatory": false
        }
        ],
        "nr_flow": [],
        "path_flow": "",
        "packages": ["node-red-contrib-boolean-logic"]
    };

    let noderedtemplateroot = JSON.parse(fs.readFileSync("./server/nodered_template.json"));

    // add all the template stuff
    nrflowcmp.nr_flow = noderedtemplateroot.flow;

    // get sensor list
    let alloutsensors = [];
    for (let stateidx in config.model.states) {
        let outsensors = JSON.parse(JSON.stringify(config.model.states[stateidx].sensors));
        // foreach is synchronous ergo the thingy will work for the index, big yeet
        outsensors.forEach(outsens => { outsens.topic = "Output"; });
        alloutsensors = alloutsensors.concat(outsensors);
    }
    // clear duplicate ids
    alloutsensors = alloutsensors.filter(function (item, pos) {
        return alloutsensors.findIndex(s => s.id === item.id) == pos;
    });
    // assign indexes
    for (let aosidx in alloutsensors) {
        alloutsensors[aosidx].index = aosidx;
    }

    let allinsensors = [];
    for (let transidx in config.model.transitions) {
        let insensors = JSON.parse(JSON.stringify(config.model.transitions[transidx].sensors));
        insensors.forEach(insens => { insens.topic = "Input"; });
        allinsensors = allinsensors.concat(insensors);
    }
    // clear duplicate ids
    allinsensors = allinsensors.filter(function (item, pos) {
        return allinsensors.findIndex(s => s.id === item.id) == pos;
    });
    // assign indexes
    for (let aisidx in allinsensors) {
        allinsensors[aisidx].index = aisidx;
    }

    // full sensor list
    let sensors = [...alloutsensors, ...allinsensors];

    //console.log(sensors);
    //finalLog(config);

    // update sync node to add orderin and orderout
    let syncnode = nrflowcmp.nr_flow.find(nn => nn.id === noderedtemplateroot.id.sync);
    syncnode.orderin = allinsensors.map(sens => config.sensors.find(s => s.id === sens.id).name).join(",");
    syncnode.orderout = alloutsensors.map(sens => config.sensors.find(s => s.id === sens.id).name).join(",");

    // generate fiohmm node
    let fiohimmnode = noderedtemplateroot.fiohmmtemplate;
    fiohimmnode.model = generateSCXML(config.model, sensors);
    nrflowcmp.nr_flow.push(fiohimmnode);

    // generate tagging nodes
    let y = 100;
    for (let sensoridx in sensors) {
        let sensor = sensors[sensoridx];
        // name shouldn't be in config sensor data retrieve from extraprops
        let sensorname = config.sensors.find(s => s.id === sensor.id).name;
        // weirdo function at the exit of the snake
        let weirdonode = {
            "id": nodeIDGenerator(),
            "type": "function",
            "z": config.id,
            "name": sensor.topic + "(" + sensorname + ")",
            "func": "var sensor = {};\nsensor.payload = msg.payload[0].m_value;\n\nsensor.topic='" + sensorname + "';\nreturn sensor;",
            "outputs": 1,
            "noerr": 0,
            "x": 300,
            "y": y,
            "wires": [[noderedtemplateroot.id.eventmgr, noderedtemplateroot.id.observations]]
        };

        // jsonizing shit 
        let jsonnode = {
            "id": nodeIDGenerator(),
            "type": "json",
            "z": config.id,
            "name": "",
            "pretty": false,
            "x": 200,
            "y": y,
            "wires": [[weirdonode.id]]
        };

        // tag node itself
        let tagnode = {
            "id": nodeIDGenerator(),
            "type": "function",
            "z": config.id,
            "name": sensorname,
            "func": "var json = {};\n\nvar name   = \"" + sensorname + "\";\njson.topic = \"" + sensor.topic + "\";\n\nvar now = new Date();\nvar timestamp = now.toISOString();\njson.payload = \"[{\\\"m_time_stamp\\\":\\\"\" + timestamp +\n    \"\\\",\\\"m_name\\\":\\\"\" + name + \n    \"\\\",\\\"m_value\\\":\\\"\" + msg.payload +\n    \"\\\"" + "}]\";\n\nreturn json;\n",
            "outputs": 1,
            "noerr": 0,
            "x": 100,
            "y": y,
            "wires": [[noderedtemplateroot.id.sync, jsonnode.id]]
        };

        //finalLog(tagnode);

        // add the smala to the flow
        nrflowcmp.nr_flow.push(weirdonode);
        nrflowcmp.nr_flow.push(jsonnode);
        nrflowcmp.nr_flow.push(tagnode);

        y += 100;
    }

    // update all the z coordinates
    nrflowcmp.nr_flow.forEach(node => node.z = config.id);

    // add new components to genesis
    template.dm.components.push(dockercmp);
    template.dm.components.push(noderedcmp);
    template.dm.components.push(nrflowcmp);

    // add containment links
    template.dm.containments.push(noderedondocker);
    template.dm.containments.push(flowonnodered);

    // process visual stuff 
    processGraph(template);

    //console.log(JSON.stringify(template));
    //finalLog(template);

    // returns
    let ret = { status: "ok", genesisModel: template };
    console.log("Export OK");
    res.end(JSON.stringify(ret));
}

function processGraph(template) {
    if (!template.graph || !template.graph.style || !template.graph.elements.nodes) {
        template.graph = JSON.parse(fs.readFileSync("./server/graphtemplate.json"));
    }
    let dockerhostnode = {
        "data": {
            "id": "BDAHost"
        },
        "position": {
            "x": 341.05549412903906,
            "y": 206.33957276984444
        },
        "group": "nodes",
        "removed": false,
        "selected": false,
        "selectable": true,
        "locked": false,
        "grabbable": true,
        "classes": "container"
    };
    let noderednode = {
        "data": {
            "id": "BDANodeRED",
            "parent": "BDAHost"
        },
        "position": {
            "x": 341.05549412903906,
            "y": 206.33957276984444
        },
        "group": "nodes",
        "removed": false,
        "selected": false,
        "selectable": true,
        "locked": false,
        "grabbable": true,
        "classes": "node_red"
    };
    let noderedflownode = {
        "data": {
            "id": "BDANodeRED-flow",
            "parent": "BDANodeRED"
        },
        "position": {
            "x": 341.05549412903906,
            "y": 206.33957276984444
        },
        "group": "nodes",
        "removed": false,
        "selected": false,
        "selectable": true,
        "locked": false,
        "grabbable": true,
        "classes": ""
    };
    template.graph.elements.nodes.push(dockerhostnode);
    template.graph.elements.nodes.push(noderednode);
    template.graph.elements.nodes.push(noderedflownode);
}

// start
app.listen(port, () => console.log(`BDA server listening on port ${port}!`))

// https://stackoverflow.com/a/2117523
function uuidgen() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function nodeIDGenerator() {
	return generateID(8) + "." + generateID(6);
}

// generate hex id https://stackoverflow.com/a/27747377
// dec2hex :: Integer -> String
function dec2hex(dec) {
	return ('0' + dec.toString(16)).substr(-2);
}

// generateId :: Integer -> String
function generateID(len) {
	var arr = new Uint8Array((len || 40) / 2);
	arr = crypto.randomBytes(arr.length);
	return Array.from(arr, dec2hex).join('');
}

function finalLog(o) {
	console.log(util.inspect(o, false, null, true));
}