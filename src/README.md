# Behavioural Drift Analysis configuration tool

## Dependencies:
- node.js v14 (tested with 10.22.1, 12.19.0 and 14.14.0). Dependencies issue with v15.
- npm v6 (tested with 5.6.0 and 6.14.8)

## Install Behavioural Drift Analysis from Sources

### Setup
Install node packages for bda-app:

    cd bda-app
    npm install

### Running
Start bda-app:

    cd bda-app
    npm start

If everything is up and running properly the BDA configuration will be available at http://localhost:3000

## Install Behavioural Drift Analyses from the public Docker image

### Setup
Pull the image:

    docker pull enactproject/bda-app:latest

### Running
Start the docker container:

    docker run -p 3000:3000/tcp enactproject/bda-app:latest

If everything is up and running properly the BDA configuration will be available at http://localhost:3000
