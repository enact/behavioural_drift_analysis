# Behavioural Drift Analysis configuration tool

Smart Iot Systems (SIS) are computing systems composed with distributed computational elements whose, when embedded in physical things, a.k.a. devices, provide these systems with an interface to the physical world through transducers (sensors and actuators).
As such, SIS can be considered as subset of the Cyber-Physical Systems (CPS).
These systems pose new challenges. Indeed, as far as physical things are concerned, no guaranty can be made on their availability on the long run. 
The underlying infrastructure of SIS can thus be volatile. 
Moreover, the purpose of some of these systems can only be achieved from interactions with the physical environment through actuators (e.g., Heating, Ventilation and Air-Conditioning (HVAC) controllers).
In this context, these systems can possibly be affected by unanticipated physical processes over which they have no control leading their behaviour to potentially drift over time in the best case or malfunction in the worst case .
Many platforms include context awareness and monitoring mechanisms.
However, these platforms do not consider behavioural drift monitoring and analysis as an awareness criterion. 

Behabioural Drift Analysis tool provides a way to monitor and analyse the drift of IoT and CPS systems.

## Table of contents
- [Setup](./src/README.md)
  - How to run the BDC configuration tool
- [Tutorials](./docs/tutorials/README.md)
  - Step by step usage examples
- [Demo 1 - Bilbao](./demos/demo_bilbao/README.md)
  - Stuff used for the first demonstration of the tool