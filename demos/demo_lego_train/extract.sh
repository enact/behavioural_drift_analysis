#!/bin/bash

FIELDS="Accelerometer6_X\|Accelerometer6_Y\|Accelerometer7_X\|Accelerometer7_Y\|TrainIntegrity"

DATE="2021-03-18_09-37-00"
FILE="Video_Lego_Train"

grep "$FIELDS" ${DATE}_${FILE}-test.ifdb | tr ',' ' ' | sed 's/value=//g' | sed 's/\./,/g' | awk '{print $3 " " $1 " " $2}' | sort > ${DATE}_Extracted_values.ifdb
