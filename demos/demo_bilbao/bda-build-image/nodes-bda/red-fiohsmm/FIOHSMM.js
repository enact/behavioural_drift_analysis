﻿module.exports = function (RED) {
    function FIOHSMMNode(config) {
        RED.nodes.createNode(this, config);

        this.assembly = config.assembly;
        this.bean = config.bean
        var node = this;

        var FIOHSMM = require('edge-js').func(
            {
                source: function () {/*
                    using System;
                    using System.Dynamic;
                    using System.Threading.Tasks;
                
                    public class StartUp {

					    Func<object,Task<object>> SendDrift { get; set; }

                        //----------
					    public void onNewDrift(string result) {
                        //----------
						    this.SendDrift(result);
					    }
                        //----------
					    public void onViterbiPath(string path) {
                        //----------
						    Console.WriteLine(path);
					    }
                        //----------
					    public void onStatus(string status) {
                        //----------
						    Console.WriteLine(status);
					    }
                        //----------
					    public async Task<object> Invoke(Func<object,Task<object>> OutputFunc) {
                        //----------
                    		SendDrift = OutputFunc;
                            var m_fiohsmm = new WComp.Beans.FIOHSMM();
                            m_fiohsmm.JsonEvent += onNewDrift;
                            m_fiohsmm.StatusEvent += onStatus;
                            //m_fiohsmm.ViterbiPathEvent += onViterbiPath;
						    return new {
                                //----------
                                SetIndex = (Func<object,Task<object>>)(
                                //----------
                                    async (index) =>
                                    {
                                        m_fiohsmm.SetIndex(Convert.ToInt16((string)index));
                                        return null;
                                    }
                                ),
                                //----------
                                SetModel = (Func<object,Task<object>>)(
                                //----------
                                    async (model) =>
                                    {
                                        m_fiohsmm.SetModel(Convert.ToString(model));
                                        return null;
                                    }
                                ),
                                //----------
                                NewSequenceAvailable = (Func<object,Task<object>>)(
                                //----------
                                    async (sequence) =>
                                    {
                                        m_fiohsmm.NewSequenceAvailable(Convert.ToString(sequence));
                                        return null;
                                    }
                                )
                            };
					    }
                    }
                */},
                references: ['FIOHSMM.dll', 'MLib.dll', 'Newtonsoft.Json.dll']
            });

        var BeanOutput = function (message, callback) {
            node.send({ payload: message });
            callback();
        }

        var fiohsmm = FIOHSMM(BeanOutput, true);

        // Properties (change taken into account after deployment)
        this.model = config.model;
        fiohsmm.SetModel(node.model);

        this.index = config.index;
        fiohsmm.SetIndex(node.index);

        // Callback
        node.on('input', function (msg) {
            fiohsmm.NewSequenceAvailable(JSON.stringify(JSON.parse(msg.payload)), true);
        });
    }
    RED.nodes.registerType("FIOHSMM", FIOHSMMNode);
}