﻿## FIOHSMM_NodeRed

#Testing node in Node-RED

You can install the node into your Node-RED runtime.
To test a node module locally, the npm link command can be used. This allows you to develop the node in a local 
directory and have it linked into a local node-red install, as if it had been npm installed.

    1. in the directory containing the node’s package.json file, run: npm link.
    2. in your node-red user directory, typically ~/.node-red run: npm link <name of node module>.

This creates the appropriate symbolic links between the two directories so Node-RED will discover the node when
it starts. Any changes to the node’s file can be picked up by simply restarting Node-RED.
