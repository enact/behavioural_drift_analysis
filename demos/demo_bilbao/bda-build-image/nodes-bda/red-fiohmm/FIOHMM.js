﻿module.exports = function (RED) {
    function FIOHMMNode(config) {
        RED.nodes.createNode(this, config);

        this.assembly = config.assembly;
        this.bean = config.bean
        var node = this;

        var FIOHMM = require('edge-js').func(
            {
                source: function () {/*
                    using System;
                    using System.Dynamic;
                    using System.Threading.Tasks;
                
                    public class StartUp {

					    Func<object,Task<object>> Result { get; set; }
                        
                        //----------
					    public void onViterbiPath(string path) {
                        //----------
                            this.Result(path);
					    }
                        //----------
					    public void onStatus(string status) {
                        //----------
						    Console.WriteLine(status);
					    }
                        //----------
					    public async Task<object> Invoke(Func<object,Task<object>> OutputFunc) {
                        //----------
                    		Result = OutputFunc;

                            var m_fiohmm = new WComp.Beans.FIOHMM();
                            m_fiohmm.PathEvent += onViterbiPath;
                            m_fiohmm.StatusEvent += onStatus;

						    return new {
                                //----------
                                SetIndex = (Func<object,Task<object>>)(
                                //----------
                                    async (index) =>
                                    {
                                        m_fiohmm.SetIndex(Convert.ToInt16((string)index));
                                        return null;
                                    }
                                ),
                                //----------
                                SetModel = (Func<object,Task<object>>)(
                                //----------
                                    async (model) =>
                                    {
                                        m_fiohmm.SetModel(Convert.ToString(model));
                                        return null;
                                    }
                                ),
                                //----------
                                NewSequenceAvailable = (Func<object,Task<object>>)(
                                //----------
                                    async (sequence) =>
                                    {
                                        m_fiohmm.NewSequenceAvailable(Convert.ToString(sequence));
                                        return null;
                                    }
                                )
                            };
					    }
                    }
                */},
                references: ['FIOHMM.dll', 'MLib.dll', 'Newtonsoft.Json.dll']
            });

        var BeanOutput = function (message, callback) {
            node.send({ payload: message });
            callback();
        }

        var fiohmm = FIOHMM(BeanOutput, true);

        // Properties (change taken into account after deployment)
        this.model = config.model;
        fiohmm.SetModel(node.model);

        this.index = config.index;
        fiohmm.SetIndex(node.index);

        // Callback
        node.on('input', function (msg) {
            fiohmm.NewSequenceAvailable(JSON.stringify(JSON.parse(msg.payload)), true);
        });
    }
    RED.nodes.registerType("FIOHMM", FIOHMMNode);
}