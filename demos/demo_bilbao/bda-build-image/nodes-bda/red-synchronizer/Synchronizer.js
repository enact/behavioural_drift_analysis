﻿module.exports = function (RED) {
    function SynchronizerNode(config) {
        RED.nodes.createNode(this, config);

        this.assembly = config.assembly;
        this.bean = config.bean
        var node = this;

        var Synchronizer = require('edge-js').func(
            {
                source: function () {/*
                    using System;
                    using System.Dynamic;
                    using System.Threading.Tasks;
                
                    public class StartUp {

					    Func<object,Task<object>> SendImpl { get; set; }

                        //----------
					    public void onNewSequence(string result) {
                        //----------
						    this.SendImpl(result);
					    }

                        //----------
					    public async Task<object> Invoke(Func<object,Task<object>> OutputFunc) {
                        //----------
                    		SendImpl = OutputFunc;
                            var m_synchronizer = new WComp.Beans.Synchronizer();
                            m_synchronizer.NewSequenceAvailableEvent += onNewSequence;
						    return new {
                               //----------
                                SequenceLength = (Func<object,Task<object>>)(
                                //----------
                                    async (length) =>
                                    {
                                        m_synchronizer.SequenceLength = Convert.ToInt16(length);
                                        return null;
                                    }
                                ),
                                //----------
                                InputSequence = (Func<object,Task<object>>)(
                                //----------
                                    async (sequence) =>
                                    {
                                        m_synchronizer.InputSequence = Convert.ToString(sequence);
                                        return null;
                                    }
                                ),
                                //----------
                                InputSequenceOrder = (Func<object,Task<object>>)(
                                //----------
                                    async (order) =>
                                    {
                                        m_synchronizer.Input_Columns_Sort = Convert.ToString(order);
                                        Console.WriteLine("Input sequence order set to : " + order);
                                        return null;
                                    }
                                ),
                                //----------
                                OutputSequence = (Func<object,Task<object>>)(
                                //----------
                                    async (sequence) =>
                                    {
                                        m_synchronizer.OutputSequence = Convert.ToString(sequence);
                                        return null;
                                    }
                                ),
                                //----------
                                OutputSequenceOrder = (Func<object,Task<object>>)(
                                //----------
                                    async (order) =>
                                    {
                                        m_synchronizer.Output_Columns_Sort = Convert.ToString(order);
                                        Console.WriteLine("Output sequence order set to : " + order);
                                        return null;
                                    }
                                ),
                                //----------
                                Accuracy = (Func<object,Task<object>>)(
                                //----------
                                    async (accuracy) =>
                                    {
                                        m_synchronizer.Accuracy = Convert.ToString(accuracy);
                                        return null;
                                    }
                                ),
                                //----------
                                SlidingWindowLength = (Func<object,Task<object>>)(
                                //----------
                                    async (slidingWindowLenght) =>
                                    {
                                        m_synchronizer.SlidingWindowLength = Convert.ToInt16(slidingWindowLenght);
                                        return null;
                                    }
                                )
                            };
					    }
                    }
                */},
                references: ['Synchronizer.dll', 'MLib.dll', 'Newtonsoft.Json.dll']
            });

        var BeanOutput = function (message, callback) {
            node.send({ payload: message });
            callback();
        }

        var synchronizer = Synchronizer(BeanOutput, true);

        // Properties (change taken into account after deployment)
        this.length = config.length;
        synchronizer.SequenceLength(node.length);

        this.orderin = config.orderin;
        synchronizer.InputSequenceOrder(node.orderin);

        this.orderout = config.orderout;
        synchronizer.OutputSequenceOrder(node.orderout);

        this.accuracy = config.accuracy;
        synchronizer.Accuracy(node.accuracy);

        this.slidingWindowLength = config.slidingWindowLength;
        synchronizer.SlidingWindowLength(node.slidingWindowLength);

        // Callback
        node.on('input', function (msg) {

            if(msg.topic == "Input")
                synchronizer.InputSequence(JSON.stringify(JSON.parse(msg.payload)), true);
            else if (msg.topic == "Output")
                synchronizer.OutputSequence(JSON.stringify(JSON.parse(msg.payload)), true);
        });
    }
    RED.nodes.registerType("Synchronizer", SynchronizerNode);
}