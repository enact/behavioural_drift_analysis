#!/bin/bash

while true 
do
    i2cdetect -y 1 | grep 04 > /dev/null
    if [ `echo $?` -eq 1 ]
    then
        echo "GrovePi Red LED ON..."
        avrdude -c gpio -p m328p > /dev/null 2> /dev/null
    fi
    sleep 0.2
done
