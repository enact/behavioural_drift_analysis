# Behavioural Drift Analysis Bilbao Demo
 
Docker image to embed all need feature for BDA (especially to execute .NET code from node-red).

Warning: The current issue is the following one: if adding new node-red package, suppress all the installed one on the docker image. So GeneSIS should not send any other request for installing node-red packages.
The following GeneSIS request to install packages:["node-red-dashboard" and "node-red-contrib-mqtt-broker"] as been removed for the moment.

To launch the demo, you just have to select the right GeneSIS-deploy-xxx file to deply the app, obs, bda or any combinaison the the previous ones.